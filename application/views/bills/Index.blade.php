@extends('layout.index')
@section('content')
    <section class="section">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row pt-3">
                            <div class="col-sm-8">
                                <div class="card border border-primary">
                                    <div class="card-header">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                Tabel Data tagihan
                                            </div>
                                            <div class="col-sm-8 text-end">
                                                <div class="btn-group btn-group-xs ">
                                                    <button class="btn btn-secondary refresh">Refresh</button>
                                                    <button class="btn btn-info empty" hidden>Kosongkan Data</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body m-3">
                                        <table class="table table-sm" id="dt-table">
                                            <thead>
                                                <tr>
                                                    <th scope="col">#</th>
                                                    <th scope="col">Jenis Tagihan</th>
                                                    <th scope="col">Jumlah Tagihan</th>
                                                    {{-- <th scope="col">Bulanan</th> --}}
                                                    <th scope="col">Active</th>
                                                    <th scope="col">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                            <div class="col-sm-4">
                                <div class="card  border border-primary">
                                    <div class="card-header">
                                        Form Data tagihan
                                    </div>
                                    <div class="card-body m-3 p-0">
                                        <form class="row g-3" id="form-tagihan">
                                            <input type="hidden" name="id_" id="id_" value="">
                                            <div class="col-12">
                                                <label for="inputNanme4" class="form-label">Nama Tagihan</label>
                                                <input type="text" class="form-control" id="jenis_tagihan"
                                                    name="jenis_tagihan" required>
                                            </div>
                                            <div class="col-12">
                                                <label for="inputNanme4" class="form-label">Jumlah Tagihan</label>
                                                <input type="text" class="form-control" id="total_tagihan"
                                                    name="total_tagihan" required>
                                            </div>
                                            
                                            <div class="col-12">
                                                <label for="inputNanme4" class="form-label">Aktif</label>
                                                <div class="form-check form-switch">
                                                    <input class="form-check-input" type="checkbox" id="active" name="active">
                                                    <label class="form-check-label" for="active"><small>Ya</small> </label>
                                                </div>
                                            </div>
                                            <div class="text-center">
                                                <button type="submit"
                                                    class="btn btn-primary btn-xs btn-save">Submit</button>
                                                <button type="reset" class="btn btn-secondary btn-xs">Reset</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </section>
    <script>
        $(function() {
            LoadTables();
            $('#total_tagihan').simpleMoneyFormat();

            function LoadTables() {
                $('#dt-table').DataTable({
                    stateSave: true,
                    destroy: true,
                    ajax: {
                        url: base_url + "data-tagihan/tables",
                        type: "POST",
                        dataSrc: "",
                    },
                    columns: [{
                            render: function(data, type, row, meta) {
                                return meta.row + 1;
                            }
                        },
                        {
                            data: "jenis_tagihan"
                        },
                        {
                            data: "total_tagihan",render:function(data){
                                return 'Rp '+$.number(parseInt(data), 0, ',', '.')
                            }
                        },
                        {
                            data: "active"
                        },
                    ],
                    columnDefs: [{
                        targets: 4,
                        data: "id_tagihan",
                        render: function(data) {
                            return '<div role="group" class="btn-group-xs btn-group btn-group-toggle" data-toggle="buttons">' +
                                '<button class="btn btn-warning edit" data-id="' + data +
                                '">Ubah</button>' +
                                '<button class="btn btn-danger hapus" data-id="' + data +
                                '">Hapus</button>' +
                                '</div>';
                        }
                    }]
                });
            }

            $('#dt-table').on('click', '.edit', function() {
                let id = $(this).data('id');
                let DataForm = $('#form-tagihan');
                let name = [];
                $.ajax({
                    type: "get",
                    url: base_url + "data-tagihan/tables",
                    data: {
                        id: id
                    },
                    dataType: "JSON",
                    success: function(response) {
                        $('#id_').val(response.id_tagihan);
                        $('#jenis_tagihan').val(response.jenis_tagihan);
                        $('#total_tagihan').val(response.total_tagihan);
                        if(response.bulanan == 1){
                            $('#bulanan').attr('checked',true);
                        }
                        if(response.active == 'Y'){
                            $('#active').attr('checked',true);
                        }
                       
                    }
                });
            });
            $("#total_tagihan").keypress(function(e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }

            });


            $('#dt-table').on('click', '.hapus', function() {
                let id = $(this).data('id');
                Swal.fire({
                    title: 'Anda Yakin?',
                    text: "Data Akan Dihapus",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            type: "post",
                            url: base_url + "data-tagihan/delete",
                            data: {
                                id: id
                            },
                            dataType: "JSON",
                            success: function(response) {
                                LoadTables();
                                if (response.success) {
                                    Swal.fire({
                                        position: 'center',
                                        icon: 'success',
                                        title: response.message,
                                        showConfirmButton: false,
                                        timer: 1500
                                    })
                                } else {
                                    Swal.fire({
                                        position: 'center',
                                        icon: 'success',
                                        title: response.message,
                                        showConfirmButton: false,
                                        timer: 1500
                                    })
                                }

                            },
                            error: function(request, textStatus, errorThrown) {
                                console.log(request.responseText);
                            }
                        });
                    }
                })

            });

            $('.refresh').on('click', function() {
                LoadTables();
            });

            $('.empty').on('click', function() {
                Swal.fire({
                    title: 'Anda Yakin?',
                    text: "Semua Data Akan Di Hapus",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            type: "get",
                            url: "data-tagihan/empty-data",
                            dataType: "JSON",
                            success: function(response) {
                                LoadTables();
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    title: response.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                })
                            }
                        });
                    }
                })

            });


            let validator = $("#form-tagihan").validate({
                rules: {
                    "jenis_tagihan": "required",
                    "total_tagihan": "required",
                    "bulanan": false,
                    "active": false,
                },
                messages: {
                    "jenis_tagihan": "Tidak Boleh Kosong",
                    "total_tagihan": "Tidak Boleh Kosong",
                    "periode_tagihan": "pilih",
                    "active": "pilih",
                },
                errorElement: "div",
                errorPlacement: function(error, element) {
                    // Add the `invalid-feedback` class to the error element
                    error.addClass("invalid-feedback");
                    if (element.hasClass("select2")) {
                        error.insertAfter(element.next("span"));
                    } else {
                        error.insertAfter(element);
                    }
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-invalid").removeClass("is-valid");
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-valid").removeClass("is-invalid");
                },
                submitHandler: Submittagihan,
            });

            function Submittagihan() {
                let btn = $('.btn-save');
                let DataForm = $('#form-tagihan');

                $.ajax({
                    type: "post",
                    url: base_url + "data-tagihan/save",
                    data: DataForm.serialize(),
                    dataType: "JSON",
                    beforeSend: function() {
                        btn.html(
                            '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Prosessing'
                        );
                        btn.attr('disabled', true);
                    },
                    success: function(response) {
                        LoadTables();
                        $('#id_').val('');
                        $('#bulanan, #active').attr('checked',false);
                        DataForm[0].reset();
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: response.message,
                            showConfirmButton: false,
                            timer: 1500
                        })
                    },
                    complete: function() {
                        $.each(DataForm.serializeArray(), function(ind, value) {
                            $('[name="' + value.name + '"]').removeClass('is-valid');
                            $('[name="' + value.name + '"]').removeClass('is-invalid');
                        });
                        setTimeout(() => {
                            btn.html('Submit');
                            btn.attr('disabled', false);
                        }, 1000);

                    },
                    error: function(request, textStatus, errorThrown) {
                        console.log(request.responseText);
                    }
                });
            }

        });
    </script>
@endsection
