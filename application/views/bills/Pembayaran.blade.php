@extends('layout.index')
@section('content')
    <section class="section">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        Cari Data
                    </div>
                    <div class="card-body mt-2">
                        <div class="row">
                            <div class="form-group col-sm-4">
                                <select name="siswa" id="siswa" class="form-control">
                                    <option value="">==Pilih Siswa</option>
                                </select>
                            </div>
                            <div class="form-group col-sm-4">
                                <select name="tagihan" id="tagihan" class="form-control">
                                    <option value="">==Pilih tagihan</option>
                                </select>
                            </div>
                            <div class="form-group col-sm-4">
                                <button class="btn btn-primary btn-md btn-cari">Cari Data</button>
                                <button class="btn btn-secondary btn-md resetfilter">Bersihkan Pencarian</button>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
            <div class="col-sm-12" id="result-page" style="display: none">
                <div class="row">
                    <div class="col-sm-3 pe-0">
                        <div class="card border border-primary">
                            <div class="card-header">
                                Detail Identitas Siswa
                            </div>
                            <div class="card-body p-0">
                                <ul class="list-group">
                                    <li class="list-group-item d-flex justify-content-between align-items-start">
                                        <div class="ms-2 me-auto">
                                            <div class="fw-bold">NIS/NISN</div>
                                            <small id="nis-nisn">Hello</small>
                                        </div>

                                    </li>
                                    <li class="list-group-item d-flex justify-content-between align-items-start">
                                        <div class="ms-2 me-auto">
                                            <div class="fw-bold">Nama Siswa</div>
                                            <small id="nama"></small>
                                        </div>

                                    </li>
                                    <li class="list-group-item d-flex justify-content-between align-items-start">
                                        <div class="ms-2 me-auto">
                                            <div class="fw-bold">Kelamin</div>
                                            <small id="kelamin"></small>
                                        </div>

                                    </li>
                                    <li class="list-group-item d-flex justify-content-between align-items-start">
                                        <div class="ms-2 me-auto">
                                            <div class="fw-bold">Kelas</div>
                                            <small id="kelas"></small>
                                        </div>

                                    </li>
                                    <li class="list-group-item d-flex justify-content-between align-items-start">
                                        <div class="ms-2 me-auto">
                                            <div class="fw-bold">Angkatan</div>
                                            <small id="angkatan"></small>
                                        </div>

                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="card border border-primary">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-sm-4">
                                        Detail Pembayaran Dan Tagihan
                                        {{-- <input type="hidden" id="id_siswa">
                                        <input type="hidden" name="" id="id_tagihan"> --}}
                                    </div>

                                </div>
                            </div>
                            <div class="card-body m-3">
                                <table class="table table-sm" id="dt-table">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Tagihan</th>
                                            <th scope="col">Nominal</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="pay-modal" data-bs-backdrop="static" data-bs-keyboard="false"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-body">
                    <form class="row g-3" id="form-payment">
                        <input type="hidden" name="id_siswa" id="id_siswa" value="">
                        <input type="hidden" name="id_tagihan" id="id_tagihan" value="">
                        <div class="col-md-12">
                            <div class="form-floating">
                                <input type="text" class="form-control" id="nama_siswa" name="nama_siswa" value=""
                                    disabled>
                                <label for="floatingName">Nama Siswa</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-floating">
                                <input type="text" class="form-control" id="jenis_tagihan" name="jenis_tagihan"
                                    placeholder="Jenis Tagihan" disabled>
                                <label for="floatingEmail">Jenis Tagihan</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-floating">
                                <input type="text" class="form-control" id="nominal_tagihan" name="nominal_tagihan"
                                    placeholder="Jenis Tagihan" disabled>
                                <label for="floatingEmail">Nominal Tagihan</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <select name="tahun" id="tahun" class="form-control">
                                    <option value="">==pilih tahun==</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-floating">
                                <select name="bulan" id="bulan" class="form-control">
                                    <option value="">==pilih bulan==</option>
                                </select>

                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-floating">
                                <input type="text" class="form-control" id="jumlah_bayar" name="jumlah_bayar"
                                    placeholder="Jumlah Bayar">
                                <label for="floatingEmail">Jumlah bayar</label>
                            </div>
                        </div>

                        <div class="text-center">
                            <button type="submit" class="btn btn-primary btn-save">Submit</button>
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    {{-- <div class="modal fade" id="pay-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" hidden>
                    <h5 class="modal-title">Large Modal</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="card mb-1 border border-primary">
                        <div class="card-header">
                            Form Pembayaran
                        </div>
                        <div class="card-body">
                            <form class="row g-3" id="form-payment">
                                <input type="hidden" name="id_siswa" id="id_siswa" value="">
                                <input type="hidden" name="id_tagihan" id="id_tagihan" value="">
                                <div class="col-md-12">
                                    <div class="form-floating">
                                        <input type="text" class="form-control" id="nama_siswa" name="nama_siswa"
                                            value="" disabled>
                                        <label for="floatingName">Nama Siswa</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-floating">
                                        <input type="text" class="form-control" id="jenis_tagihan" name="jenis_tagihan"
                                            placeholder="Jenis Tagihan" disabled>
                                        <label for="floatingEmail">Jenis Tagihan</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="nominal_tagihan"
                                            name="nominal_tagihan" placeholder="Jenis Tagihan" disabled>
                                        <label for="floatingEmail">Nominal Tagihan</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select name="tahun" id="tahun" class="form-control">
                                            <option value="">==pilih tahun==</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-floating">
                                        <select name="bulan" id="bulan" class="form-control">
                                            <option value="">==pilih bulan==</option>
                                        </select>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-floating">
                                        <input type="text" class="form-control" id="jumlah_bayar" name="jumlah_bayar"
                                            placeholder="Jumlah Bayar">
                                        <label for="floatingEmail">Jumlah bayar</label>
                                    </div>
                                </div>
                                
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary btn-save">Submit</button>
                                    <button type="button" class="btn btn-secondary"
                                        data-bs-dismiss="modal">Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" hidden>

                    <button type="button" class="btn btn-primary ">Save changes</button>
                </div>
            </div>
        </div>
    </div> --}}
    <script>
        let sisa_bayar = 0;
        const monthNames = ["Januari", "Febuari", "Maret", "April", "Mei", "Juni", "Juli", "Agusturs", "September",
            "Oktober", "November", "Desember"
        ];
        const years = [];
        const months = [];
        const currentYear = (new Date()).getFullYear();
        const currentMonth = new Date().getMonth();

        $(function() {
            let table;
            $('#jumlah_bayar').simpleMoneyFormat();
            $('#nominal_tagihan').simpleMoneyFormat();

            $('.resetfilter').on('click', function() {
                $("#siswa, #tagihan").val(null).trigger("change");
                LoadTables();
                $('#result-page').css('display', 'none');
            });
            $('#siswa').select2({
                width: '100%',
                allowClear: true,
                theme: 'bootstrap-5',
                placeholder: 'Pilih Siswa',
                ajax: {
                    dataType: 'json',
                    url: base_url + 'data-siswa/tables',
                    type: 'GET',
                    data: function(params) {
                        return {
                            search: params.term
                        }
                    },
                    processResults: function(data, page) {
                        let list = [];
                        $.each(data, function(index, val) {
                            list.push({
                                'id': val.id_siswa,
                                'text': val.nama_siswa + ' - ' + val.nis
                            });
                        });
                        return {
                            results: list
                        };
                    },
                }
            });
            $('#tagihan').select2({
                width: '100%',
                allowClear: true,
                theme: 'bootstrap-5',
                placeholder: 'Pilih Tagihan',
                ajax: {
                    dataType: 'json',
                    url: base_url + 'data-tagihan/tables',
                    type: 'GET',
                    data: function(params) {
                        return {
                            search: params.term
                        }
                    },
                    processResults: function(data, page) {
                        let list = [];
                        $.each(data, function(index, val) {
                            list.push({
                                'id': val.id_tagihan,
                                'text': val.jenis_tagihan
                            });
                        });
                        return {
                            results: list
                        };
                    },
                }
            });

            for (var i = currentYear; i >= 2015; i--) {
                years.push({
                    'id': i,
                    'text': i
                });
            }

            for (var month = 0; month <= 11; month++) {
                bln = ("0" + (month + 1)).slice(-2);
                months.push({
                    'id': bln,
                    'text': monthNames[month]
                });
            }

            $('#tahun').select2({
                width: '100%',
                theme: 'bootstrap-5',
                dropdownParent: "#pay-modal",
                allowClear: true,
                placeholder: '==Pilih tahun==',
                data: years
            });

            $('#bulan').select2({
                width: '100%',
                theme: 'bootstrap-5',
                dropdownParent: "#pay-modal",
                allowClear: true,
                placeholder: '==Pilih Bulan==',
                data: months
            });

            $('.btn-cari').on('click', function() {
                if ($('#siswa').val() == '') {
                    Swal.fire(
                        'Required',
                        'Pilih Siswa Dahulu',
                        'error'
                    )
                } else if ($('#tagihan').val() == '') {
                    Swal.fire(
                        'Required',
                        'Pilih Tagihan',
                        'error'
                    )
                } else {
                    $('#result-page').css('display', 'block');
                    LoadTables();
                    DataSiswa($('#siswa').val());
                }

                // $('#result-page').attr('hidden', false);
            });

            function DataSiswa(id) {
                $.ajax({
                    type: "GET",
                    url: base_url + "data-siswa/tables",
                    data: {
                        id: id
                    },
                    dataType: "JSON",
                    success: function(response) {
                        $('#nis-nisn').text(response.nis + '/' + response.nisn);
                        $('#nama').text(response.nama_siswa);
                        $('#nama_siswa').val(response.nama_siswa);
                        $('#kelamin').text((response.kelamin == 'L' ? 'Laki-laki' : 'Perempuan'));
                        $('#kelas').text(response.nama_kelas);
                        $('#angkatan').text(response.nama_angkatan);
                    }
                });
            }

            function LoadTables() {

                table = $('#dt-table').DataTable({
                    paging: false,
                    ordering: false,
                    info: false,
                    stateSave: true,
                    destroy: true,
                    ajax: {
                        url: base_url + "data-pembayaran/tables",
                        type: "POST",
                        data: function(data) {
                            data.siswa = $('#siswa').val();
                            data.tagihan = $('#tagihan').val();
                        }
                    },
                    columns: [{
                            render: function(data, type, row, meta) {
                                return meta.row + 1;
                            }
                        },
                        {
                            data: "tagihan"
                        },
                        {
                            data: "nominal",
                            render: function(data) {
                                return 'Rp ' + $.number(parseInt(data), 0, ',', '.')
                            }
                        },
                        {
                            data: "status"
                        },
                    ],
                    columnDefs: [{
                        targets: 4,
                        data: {
                            id_siswa: "id_siswa",
                            id_tagihan: "id_tagihan",
                            tagihan: "tagihan",
                            nominal: "nominal",
                            stats: "stats"
                        },
                        render: function(data) {
                            let disable = (data.stats == 1 ? 'disabled' : '');
                            return '<div role="group" class="btn-group-xs btn-group btn-group-toggle" data-toggle="buttons">' +
                                '<button class="btn btn-info lihat" hidden data-id="' + data.id_siswa +
                                '" >lihat </button>' +
                                '<button class="btn btn-warning bayar" data-tagihan="' + data
                                .id_tagihan + ':' + data.tagihan + ':' + data.nominal +
                                '" data-id="' + data
                                .id_siswa +
                                '" ' + disable + '>bayar</button>' +
                                '<button class="btn btn-secondary cetak" data-tagihan="' + data
                                .id_tagihan + ':' + data.tagihan + ':' + data.nominal +
                                '" data-id="' + data
                                .id_siswa +
                                '">Cetak</button>' +
                                '</div>';
                        }
                    }]


                }).on('draw.dt', function() {
                    table.rows().every(function() {
                        this.child(format(this.data())).show();
                        this.nodes().to$().addClass('shown');
                        // this next line removes the padding from the TD in the child row 
                        // In my case this gives a more uniform appearance of the data
                        this.child().find('td:first-of-type').addClass('child-container')
                    });
                });

            }


            $('#dt-table tbody').on('click', '.lihat', function() {
                var tr = $(this).closest('tr');
                var row = table.row(tr);
                if (row.child.isShown()) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                } else {
                    // Open this row
                    row.child(format(row.data())).show();
                    tr.addClass('shown');
                }
            });

            $('#dt-table').on('click', '.bayar', function() {
                let id = $(this).data('id');
                let tagihan = $(this).data('tagihan').split(":");

                $('#pay-modal').modal('show');
                $.ajax({
                    type: "GET",
                    url: base_url + "data-siswa/tables",
                    data: {
                        id: id
                    },
                    dataType: "JSON",
                    success: function(response) {
                        $('#id_siswa').val(id);
                        $('#id_tagihan').val(tagihan[0]);
                        $('#nama_siswa').val(response.nama_siswa);
                        $('#jenis_tagihan').val(tagihan[1]);
                        $('#nominal_tagihan').val($.number(parseInt(sisa_bayar), 0, ',', ','));
                        $('#tahun').val(null).trigger('change');
                        $('#bulan').val(null).trigger('change');

                    }
                });
            });

            $('#dt-table').on('click', '.btn-hapus', function() {
                let id = $(this).data('id');
                Swal.fire({
                    title: 'Yakin Ingin Menghapus Data Ini?',
                    text: "Jika Dihapus Data Tidak Dapat Dipulihkan Kembali",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        Swal.fire({
                            title: 'Enter your password',
                            input: 'password',
                            inputLabel: 'Password',
                            inputPlaceholder: 'Enter your password',
                            inputAttributes: {
                                maxlength: 10,
                                autocapitalize: 'off',
                                autocorrect: 'off'
                            }
                        }).then((result) => {
                            if (result.isConfirmed) {
                                $.ajax({
                                    type: "POST",
                                    url: base_url + "data-pembayaran/delete",
                                    data: {
                                        id: id,
                                        password: result.value
                                    },
                                    dataType: "JSON",
                                    success: function(response) {
                                        if (response.success) {
                                            LoadTables();
                                            Swal.fire({
                                                position: 'center',
                                                icon: 'success',
                                                title: response.message,
                                                showConfirmButton: false,
                                                timer: 1500
                                            })
                                        } else {
                                            Swal.fire({
                                                position: 'center',
                                                icon: 'error',
                                                title: response.message,
                                                showConfirmButton: false,
                                                timer: 1500
                                            })
                                        }

                                    },
                                    error: function(request, textStatus,
                                        errorThrown) {
                                        console.log(request.responseText);
                                    }
                                });
                            }
                        })
                    }
                })
            });

            $('#jumlah_bayar').keyup(function(e) {
                let dibayar = $(this).val();
                let jumlah = parseFloat(dibayar.replace(/,/g, ''));
                let nominal = parseFloat($('#nominal_tagihan').val().replace(/,/g, ''));
                if (jumlah > nominal) {

                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: 'Jumlah Bayar Tidak Boleh melebihi Nominal Tagihan',
                        showConfirmButton: true,
                        // timer: 1500
                    }).then((result) => {
                        $('#jumlah_bayar').val('');
                    })
                } else {
                    let sisa = nominal - jumlah;

                    // $('#sisa').val($.number(parseInt(sisa), 0, ',', ','));
                }

                // console.log(sisa);
            });

            $("#form-payment").validate({
                rules: {
                    "tahun": "required",
                    "bulan": "required",
                    "jumlah_bayar": "required",
                },
                messages: {
                    "tahun": "Tahun Tidak Boleh Kosong",
                    "bulan": "Bulan Tidak Boleh Kosong",
                    "jumlah_bayar": "Jumlah Bayar Tidak Boleh kosong",
                },
                errorElement: "div",
                errorPlacement: function(error, element) {
                    // Add the `invalid-feedback` class to the error element
                    error.addClass("invalid-feedback");
                    if (element.hasClass("select2")) {
                        error.insertAfter(element.next("span"));
                    } else {
                        error.insertAfter(element);
                    }
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-invalid").removeClass("is-valid");
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-valid").removeClass("is-invalid");
                },
                submitHandler: SubmitPayment,
            });

            function SubmitPayment() {
                let DataForm = $('#form-payment');
                let btn = $('.btn-save');

                $.ajax({
                    type: "POST",
                    url: base_url + "data-pembayaran/save",
                    data: DataForm.serialize(),
                    dataType: "JSON",
                    beforeSend: function() {
                        btn.html(
                            '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Prosessing'
                        );
                        btn.attr('disabled', true);
                    },
                    success: function(response) {
                        console.log(response);
                        DataForm[0].reset();
                        LoadTables()
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: response.message,
                            showConfirmButton: false,
                            timer: 1500
                        })
                    },
                    complete: function() {
                        $.each(DataForm.serializeArray(), function(ind, value) {
                            $('[name="' + value.name + '"]').removeClass('is-valid');
                            $('[name="' + value.name + '"]').removeClass('is-invalid');
                        });
                        setTimeout(() => {
                            $('#pay-modal').modal('hide');
                            btn.html('Submit');
                            btn.attr('disabled', false);
                        }, 1500);

                    },
                    error: function(request, textStatus, errorThrown) {
                        console.log(request.responseText);
                    }
                });
            }

            function format(d) {
                let total_bayar = 0;
                let table = '<table class="table">' +
                    '<thead><tr>' +
                    '<th>Tanggal Pembayaran</th>' +
                    '<th>Dibayar</th>' +
                    '<th>tahun Bulan Bayar</th>' +
                    '<th>Operator</th>' +
                    '<th>Aksi</th>' +
                    '</tr></thead>' +
                    '<tbody>';
                $.each(d.rincian, function(index, val) {
                    total_bayar += parseInt(val.di_bayar);
                    table += '<tr>' +
                        '<td>' + val.tgl_pembayaran + '</td>' +
                        '<td> Rp ' + $.number(parseInt(val.di_bayar), 0, ',', '.') + '</td>' +
                        '<td>' + val.bulan + '</td>' +
                        '<td>' + val.operator + '</td>' +
                        '<td><button class="btn btn-xs btn-danger btn-hapus" data-id="' + val
                        .id_pembayaran +
                        '">Hapus</button><button class="btn btn-xs btn-secondary btn-cetak" data-id="' + val
                        .id_pembayaran + '">Cetak</button></td>' +
                        '</tr>';
                });
                sisa_bayar = parseInt(d.nominal) - total_bayar;
                table += '</tbody>' +
                    '<tfoot>' +
                    '<tr>' +
                    '<td >Total Bayar</td>' +
                    '<td > Rp ' + $.number(parseInt(total_bayar), 0, ',', '.') + '</td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td >Sisa Pembayaran</td>' +
                    '<td > Rp ' + $.number(parseInt(sisa_bayar), 0, ',', '.') + '</td>' +
                    '</tr>' +
                    '</tfoot>'
                '</table>';

                return table;

            }


        });
    </script>
@endsection
