@extends('layout.index')
@section('content')
    <section class="section profile">
        <div class="row">
            <div class="col-xl-4">

                <div class="card">
                    <div class="card-body profile-card pt-4 d-flex flex-column align-items-center">

                        <img src="@asset('')assets/img/{{ $profile->logo }}" alt="Profile" class="rounded-circle"
                            id="output">
                        <h2>{{ $profile->nama_sekolah }}</h2>
                    </div>
                </div>

            </div>

            <div class="col-xl-8">

                <div class="card">
                    <div class="card-body pt-3">
                        <!-- Bordered Tabs -->
                        <ul class="nav nav-tabs nav-tabs-bordered">

                            <li class="nav-item">
                                <button class="nav-link active" data-bs-toggle="tab"
                                    data-bs-target="#profile-overview">Overview</button>
                            </li>

                            <li class="nav-item">
                                <button class="nav-link" data-bs-toggle="tab" data-bs-target="#profile-edit">Edit
                                    Profile Sekolah</button>
                            </li>

                        </ul>
                        <div class="tab-content pt-2">

                            <div class="tab-pane fade show active profile-overview" id="profile-overview">
                                <h5 class="card-title">Deskripsi Sekolah</h5>
                                <p class="small fst-italic">{{ $profile->deskripsi }}</p>

                                <h5 class="card-title">Profile Details</h5>

                                <div class="row">
                                    <div class="col-lg-3 col-md-4 label ">Nama Sekolah</div>
                                    <div class="col-lg-9 col-md-8">{{ $profile->nama_sekolah }}</div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-3 col-md-4 label">Alamat</div>
                                    <div class="col-lg-9 col-md-8">{{ $profile->alamat }}</div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-3 col-md-4 label">Website</div>
                                    <div class="col-lg-9 col-md-8">{{ $profile->website }}</div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-3 col-md-4 label">Email</div>
                                    <div class="col-lg-9 col-md-8">{{ $profile->email }}</div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-3 col-md-4 label">Telepone</div>
                                    <div class="col-lg-9 col-md-8">{{ $profile->telepon }}</div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-3 col-md-4 label">Pimpinan</div>
                                    <div class="col-lg-9 col-md-8">{{ $profile->pimpinan }}</div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3 col-md-4 label">Kepsek MTS</div>
                                    <div class="col-lg-9 col-md-8">{{ $profile->kepsek_mts }}</div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3 col-md-4 label">Kepsek MA</div>
                                    <div class="col-lg-9 col-md-8">{{ $profile->kepsek_ma }}</div>
                                </div>

                            </div>

                            <div class="tab-pane fade profile-edit pt-3" id="profile-edit">

                                <!-- Profile Edit Form -->
                                <form action="{{ base_url('data-sekolah/save') }}" method="post"
                                    enctype="multipart/form-data">
                                    <div class="row mb-3">
                                        <label for="profileImage" class="col-md-4 col-lg-3 col-form-label">Logo
                                            Sekolah</label>
                                        <div class="col-md-8 col-lg-9">
                                            <input type="file" name="logo" id="logo" class="form-control"
                                                accept="image/*" onchange="loadFile(event)">
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <label for="fullName" class="col-md-4 col-lg-3 col-form-label">Nama Sekolah</label>
                                        <div class="col-md-8 col-lg-9">
                                            <input name="nama_sekolah" type="text" class="form-control" id="fullName"
                                                value="{{ $profile->nama_sekolah }}">
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <label for="about" class="col-md-4 col-lg-3 col-form-label">Deskripsi
                                            Sekolah</label>
                                        <div class="col-md-8 col-lg-9">
                                            <textarea name="deskripsi" class="form-control" id="about" style="height: 100px">{{ $profile->deskripsi }}</textarea>
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <label for="company" class="col-md-4 col-lg-3 col-form-label">Alamat</label>
                                        <div class="col-md-8 col-lg-9">
                                            <input name="alamat" type="text" class="form-control" id="company"
                                                value="{{ $profile->alamat }}">
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <label for="Job" class="col-md-4 col-lg-3 col-form-label">Website</label>
                                        <div class="col-md-8 col-lg-9">
                                            <input name="website" type="text" class="form-control" id="Job"
                                                value="{{ $profile->website }}">
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <label for="Country" class="col-md-4 col-lg-3 col-form-label">Email</label>
                                        <div class="col-md-8 col-lg-9">
                                            <input name="email" type="text" class="form-control" id="Country"
                                                value="{{ $profile->email }}">
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <label for="Address" class="col-md-4 col-lg-3 col-form-label">Telepon</label>
                                        <div class="col-md-8 col-lg-9">
                                            <input name="telepon" type="text" class="form-control" id="Address"
                                                value="{{ $profile->telepon }}">
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <label for="Phone" class="col-md-4 col-lg-3 col-form-label">Pimpinan</label>
                                        <div class="col-md-8 col-lg-9">
                                            <input name="pimpinan" type="text" class="form-control" id="Phone"
                                                value="{{ $profile->pimpinan }}">
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <label for="Email" class="col-md-4 col-lg-3 col-form-label">Kepsek MTS</label>
                                        <div class="col-md-8 col-lg-9">
                                            <input name="kepsek_mts" type="text" class="form-control" id="Email"
                                                value="{{ $profile->kepsek_mts }}">
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <label for="Twitter" class="col-md-4 col-lg-3 col-form-label">Kepsek MA</label>
                                        <div class="col-md-8 col-lg-9">
                                            <input name="kepsek_ma" type="text" class="form-control" id="Twitter"
                                                value="{{ $profile->kepsek_ma }}">
                                        </div>
                                    </div>

                                    <div class="text-center">
                                        <button type="submit" class="btn btn-primary">Save Changes</button>
                                    </div>
                                </form><!-- End Profile Edit Form -->

                            </div>


                        </div><!-- End Bordered Tabs -->

                    </div>
                </div>

            </div>
        </div>
    </section>
    @if (flashdata('error'))
        <script>
            $(function() {
                Swal.fire({
                    position: 'top-end',
                    icon: 'error',
                    title: "{{ flashdata('error') }} ",
                    showConfirmButton: false,
                    timer: 1500
                })
            });
        </script>
    @endif
    @if (flashdata('success'))
        <script>
            $(function() {
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: "{{ flashdata('success') }} ",
                    showConfirmButton: false,
                    timer: 1500
                })
            });
        </script>
    @endif
    <script>
        var loadFile = function(event) {
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(event.target.files[0]);
            output.onload = function() {
                URL.revokeObjectURL(output.src) // free memory
            }
        };
    </script>
@endsection
