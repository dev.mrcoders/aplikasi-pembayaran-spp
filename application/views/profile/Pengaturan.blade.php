@extends('layout.index')
@section('content')
    <section class="section">
        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-lg-6">

                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Pengaturan Sistem</h5>

                        <!-- Horizontal Form -->
                        <form action="{{ base_url('pengaturan/save') }}" method="post">
                            <input type="hidden" name="id" value="{{ $setting->id_pengaturan }}">
                            <div class="row mb-3">
                                <label for="inputEmail3" class="col-sm-4 col-form-label">Background Header</label>
                                <div class="col-sm-8">
                                    <input type="color" class="form-control" id="inputText" name="bg_header" value="{{ $setting->bg_header }}">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="inputEmail3" class="col-sm-4 col-form-label">Text Color Header</label>
                                <div class="col-sm-8">
                                    <input type="color" class="form-control" id="inputText" name="color_text" value="{{ $setting->color_text }}">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="inputEmail3" class="col-sm-4 col-form-label">Background Aside</label>
                                <div class="col-sm-8">
                                    <input type="color" class="form-control" id="inputEmail" name="bg_aside" value="{{ $setting->bg_aside }}">
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="reset" class="btn btn-secondary">Reset</button>
                            </div>
                        </form><!-- End Horizontal Form -->

                    </div>
                </div>



            </div>
        </div>
    </section>
    @if (flashdata('success'))
        <script>
            $(function() {
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: "{{ flashdata('success') }} ",
                    showConfirmButton: false,
                    timer: 1500
                })
            });
        </script>
    @endif
@endsection
