<script>
    $(function() {
        $('#dt-table').DataTable({
            stateSave: true,
            destroy: true,
            ajax: {
                url: url + "data-kelas/tables",
                type: "POST",
                dataSrc: "",
            },
            columns: [{
                    render: function(data, type, row, meta) {
                        return meta.row + 1;
                    }
                },
                {
                    data: "nama_kelas"
                }
            ],
            columnDefs: [{
                targets: 2,
                data: "id_kelas",
                render: function(data) {
                    return '<div role="group" class="btn-group-xs btn-group btn-group-toggle" data-toggle="buttons">' +
                        '<button class="btn btn-warning edit" data-id="' + data +
                        '">Ubah</button>' +
                        '<button class="btn btn-danger hapus" data-id="' + data +
                        '">Hapus</button>' +
                        '</div>';
                }
            }]
        });

        $('.tambah').on('click', function() {
            // $('#exampleModal').modal('show');
        });
    });
</script>