@extends('layout.index')
@section('content')
    <style>
        table tr td {
            vertical-align: middle;
        }
    </style>
    <section class="section">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row pt-3">
                            <div class="col-sm-8">
                                <div class="card border border-primary">
                                    <div class="card-header">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                Tabel Data Pengguna
                                            </div>
                                            <div class="col-sm-8 text-end">
                                                <div class="btn-group btn-group-xs ">
                                                    <button class="btn btn-secondary refresh">Refresh</button>
                                                    <button class="btn btn-info empty" hidden>Kosongkan Data</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body m-3">
                                        <table class="table table-sm" id="dt-table">
                                            <thead>
                                                <tr>
                                                    <th scope="col">#</th>
                                                    <th scope="" width="5%">Foto</th>
                                                    <th scope="col">Nama</th>
                                                    <th scope="col">Username</th>
                                                    <th scope="col">User Level</th>
                                                    <th scope="col">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                            <div class="col-sm-4">
                                <div class="card  border border-primary">
                                    <div class="card-header">
                                        Form Data Pengguna
                                    </div>
                                    <div class="card-body m-3">
                                        <form class="row g-3" id="form-pengguna" method="POST"
                                            enctype="multipart/form-data">
                                            <input type="hidden" name="id_" id="id_" value="">
                                            <div class="col-12">
                                                <label for="inputNanme4" class="form-label">Nama Lengkap</label>
                                                <input type="text" class="form-control" id="nama" name="nama"
                                                    required>
                                            </div>
                                            <div class="col-12">
                                                <label for="inputNanme4" class="form-label">Foto</label>
                                                <input type="file" class="form-control" id="foto" name="foto">
                                            </div>
                                            <div class="col-12">
                                                <label for="inputNanme4" class="form-label">Username</label>
                                                <input type="text" class="form-control" id="username" name="username"
                                                    required>
                                            </div>
                                            <div class="col-12">
                                                <label for="inputNanme4" class="form-label">Password</label>
                                                <input type="text" class="form-control" id="password" name="password">
                                            </div>
                                            <div class="col-12">
                                                <label for="inputNanme4" class="form-label">User Level</label>
                                                <select name="role" id="role" class="form-control">
                                                    <option value="">==Pilih Role==</option>
                                                </select>
                                            </div>
                                            <div class="text-center">
                                                <button type="submit"
                                                    class="btn btn-primary btn-xs btn-save">Submit</button>
                                                <button type="reset" class="btn btn-secondary btn-xs">Reset</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </section>
    <script>
        let role;
        $(function() {
            LoadTables();
            let req = true;

            function LoadTables() {
                $('#dt-table').DataTable({
                    stateSave: true,
                    destroy: true,
                    ajax: {
                        url: base_url + "data-pengguna/tables",
                        type: "POST",
                        dataSrc: "",
                    },
                    columns: [{
                            render: function(data, type, row, meta) {
                                return meta.row + 1;
                            }
                        },
                        {
                            data: "foto",
                            render: function(data) {
                                if (data == '' || data == null) {
                                    return '<img src="' + base_url +
                                        'assets/img/user-2.jpg" class="img-fluid img-thumbnail" style=""/>';
                                } else {
                                    return '<img src="' + base_url + 'assets/img/' + data +
                                        '" class="img-fluid img-thumbnail"/>';
                                }
                            }
                        },
                        {
                            data: "nama"
                        },
                        {
                            data: "username"
                        },
                        {
                            data: "nama_role"
                        }

                    ],
                    columnDefs: [{
                        targets: 5,
                        data: {
                            id_user: "id_user",
                            id_role: "id_role"
                        },
                        render: function(data) {
                            let Disabled = (data.id_role == 1 ? 'disabled' : '');
                            return '<div role="group" class="btn-group-xs btn-group btn-group-toggle" data-toggle="buttons">' +
                                '<button class="btn btn-warning edit" data-id="' + data
                                .id_user +
                                '">Ubah</button>' +
                                '<button class="btn btn-danger hapus" data-id="' + data
                                .id_user +
                                '" ' + Disabled + '>Hapus</button>' +
                                '</div>';
                        }
                    }]
                });
            }

            $.get(base_url + "data-role/tables",
                function(data, textStatus, jqXHR) {
                    $.each(data, function(index, value) {
                        role += '<option value="' + value.id_role + '">' + value.nama_role +
                            '</option>';
                    });
                    $('#role').append(role);
                },
                "json"
            );

            $('#dt-table').on('click', '.edit', function() {
                let id = $(this).data('id');

                $.ajax({
                    type: "get",
                    url: base_url + "data-pengguna/tables",
                    data: {
                        id: id
                    },
                    dataType: "JSON",
                    success: function(response) {
                        $('#id_').val(id);
                        $('#nama').val(response.nama);
                        $('#username').val(response.username);
                        $('#role').val(response.role_id).trigger('change');
                        req = false;
                    }
                });
            });

            $('#dt-table').on('click', '.hapus', function() {
                let id = $(this).data('id');
                Swal.fire({
                    title: 'Anda Yakin?',
                    text: "Data Akan Dihapus",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            type: "post",
                            url: base_url + "data-pengguna/delete",
                            data: {
                                id: id
                            },
                            dataType: "JSON",
                            success: function(response) {
                                LoadTables();
                                if (response.success) {
                                    Swal.fire({
                                        position: 'center',
                                        icon: 'success',
                                        title: response.message,
                                        showConfirmButton: false,
                                        timer: 1500
                                    })
                                } else {
                                    Swal.fire({
                                        position: 'center',
                                        icon: 'success',
                                        title: response.message,
                                        showConfirmButton: false,
                                        timer: 1500
                                    })
                                }

                            },
                            error: function(request, textStatus, errorThrown) {
                                console.log(request.responseText);
                            }
                        });
                    }
                })

            });

            $('.refresh').on('click', function() {
                LoadTables();
            });


            $.validator.addMethod('filesize', function(value, element, param) {
                return this.optional(element) || (element.files[0].size <= param * 1000000)
            }, 'File size must be less than {0} MB');

            $("#form-pengguna").validate({
                rules: {
                    "nama": "required",
                    "foto": {
                        required: false,
                        accept: "image/jpg,image/jpeg",
                        extension: "jpeg|jpg",
                        filesize: 5
                    },
                    "username": "required",
                    "password": {
                        reuired: false
                    },
                    "role": "required",
                },
                messages: {
                    "nama": "Wajib Diisi",
                    "foto": {
                        required: "Wajib Di isi",
                        accept: "Pilih Sesuai format jpeg atau jpg",
                        extension: "pilih file yang sesuai format"
                    },
                    "username": "wajib diisi",
                    "password": {
                        required: "wajib Isi"
                    },
                    "role": "wajib diisi",
                },
                errorElement: "div",
                errorPlacement: function(error, element) {
                    // Add the `invalid-feedback` class to the error element
                    error.addClass("invalid-feedback");
                    if (element.hasClass("select2")) {
                        error.insertAfter(element.next("span"));
                    } else {
                        error.insertAfter(element);
                    }
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-invalid").removeClass("is-valid");
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-valid").removeClass("is-invalid");
                },
                submitHandler: SubmitPengguna,
            });

            function SubmitPengguna() {
                let btn = $('.btn-save');
                let DataForm = $('#form-pengguna');
                let Form = $('#form-pengguna')[0];
                let NewForm = new FormData(Form);

                $.ajax({
                    type: "post",
                    url: base_url + "data-pengguna/save",
                    data: NewForm,
                    processData: false,
                    contentType: false,
                    cache: false,
                    async: false,
                    dataType: "JSON",
                    beforeSend: function() {
                        btn.html(
                            '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Prosessing'
                        );
                        btn.attr('disabled', true);
                    },
                    success: function(response) {
                        console.log(response);
                        LoadTables();
                        DataForm[0].reset();
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: response.message,
                            showConfirmButton: false,
                            timer: 1500
                        })
                    },
                    complete: function() {
                        $.each(DataForm.serializeArray(), function(ind, value) {
                            console.log(value);
                            $('[name="' + value.name + '"]').removeClass('is-valid');
                            $('[name="' + value.name + '"]').removeClass('is-invalid');
                        });
                        $('[name="foto"]').removeClass('is-valid');
                        $('[name="foto"]').removeClass('is-invalid');
                        setTimeout(() => {
                            btn.html('Submit');
                            btn.attr('disabled', false);
                        }, 1000);

                    },
                    error: function(request, textStatus, errorThrown) {
                        console.log(request.responseText);
                    }
                });
            }


        });
    </script>
@endsection
