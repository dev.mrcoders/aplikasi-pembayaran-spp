@extends('layout.index')
@section('content')
    <section class="section">
        <div class="row">
            <div class="col-lg-12">


                <div class="card border border-primary">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-sm-4">
                                Tabel Data Siswa
                            </div>
                            <div class="col-sm-8 text-end">
                                <div class="btn-group btn-group-xs ">
                                    <button class="btn btn-primary tambah">Tambah</button>
                                    <button class="btn btn-secondary refresh">Refresh</button>
                                    <button class="btn btn-info empty" hidden>Kosongkan Data</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body m-3">
                        <table class="table table-sm" id="dt-table">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">NISN/NIS</th>
                                    <th scope="col">Nama Siswa</th>
                                    <th scope="col">Kelamin</th>
                                    <th scope="col">siswa Kelas</th>
                                    <th scope="col">Orang Tua/Wali</th>
                                    <th scope="col">Alamat</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="add-modal" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" hidden>
                    <h5 class="modal-title">Large Modal</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="card mb-1 border border-primary">
                        <div class="card-header">
                            Form Data siswa
                        </div>
                        <div class="card-body">
                            <form class="row g-3" id="form-siswa">
                                <input type="hidden" name="id_" id="id_" value="">
                                <div class="col-md-12">
                                    <div class="form-floating">
                                        <input type="text" class="form-control" id="nam_siswa" name="nama_siswa"
                                            placeholder="Nama Siswa">
                                        <label for="floatingName">Nama Siswa</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-floating">
                                        <input type="text" class="form-control" id="nisn" name="nisn"
                                            placeholder="NISN">
                                        <label for="floatingEmail">NISN</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-floating">
                                        <input type="text" class="form-control" id="nis" name="nis"
                                            placeholder="NIS">
                                        <label for="floatingPassword">NIS</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-floating mb-3">
                                        <select class="form-select" id="kelamin" name="kelamin" aria-label="State">
                                            <option value="">==Pilih Kelamin==</option>
                                            <option value="P">Perempuan</option>
                                            <option value="L">Laki - Laki</option>
                                        </select>
                                        <label for="floatingSelect">Jenis Kelamin</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-floating mb-3">
                                        <select class="form-select" id="angkatan" name="id_angkatan" aria-label="State">
                                            <option value="">==Piliah Angakatan==</option>

                                        </select>
                                        <label for="floatingSelect">Angkatan</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-floating mb-3">
                                        <select class="form-select" id="kelas" name="id_kelas" aria-label="State">
                                            <option value="">==Piliah Kelas==</option>

                                        </select>
                                        <label for="floatingSelect">Kelas</label>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-floating">
                                        <textarea class="form-control" placeholder="Address" id="alamat" name="alamat" style="height: 100px;"></textarea>
                                        <label for="floatingTextarea">Alamat</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="col-md-12">
                                        <div class="form-floating">
                                            <input type="text" class="form-control" id="nama_ayah" name="nama_ayah"
                                                placeholder="Nama ayah">
                                            <label for="floatingCity">Nama Ayah</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-floating">
                                        <input type="text" class="form-control" id="nama_ibu" name="nama_ibu"
                                            placeholder="Nama Ibu">
                                        <label for="floatingZip">Nama Ibu</label>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary btn-save">Submit</button>
                                    <button type="reset" class="btn btn-secondary">Reset</button>
                                    <button type="button" class="btn btn-secondary"
                                        data-bs-dismiss="modal">Close</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" hidden>

                    <button type="button" class="btn btn-primary ">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        let kelas;
        let angkatan;
        $(function() {
            LoadTables();

            function LoadTables() {
                $('#dt-table').DataTable({
                    stateSave: true,
                    destroy: true,
                    ajax: {
                        url: base_url + "data-siswa/tables",
                        type: "POST",
                        dataSrc: "",
                    },
                    columns: [{
                            render: function(data, type, row, meta) {
                                return meta.row + 1;
                            }
                        },
                        {
                            data: {
                                nis: "nis",
                                nisn: "nisn",
                            },
                            render: function(d) {
                                return d.nisn + ' - ' + d.nis;
                            }
                        },
                        {
                            data: "nama_siswa"
                        },
                        {
                            data: "kelamin"
                        },
                        {
                            data: {
                                nama_angkatan: "nama_angkatan",
                                nama_kelas: "nama_kelas",
                            },
                            render: function(d) {
                                return d.nama_angkatan + ' - ' + d.nama_kelas;
                            }
                        },
                        {
                            data: {
                                nama_ibu: "nama_ibu",
                                nama_ayah: "nama_ayah",
                            },
                            render: function(d) {
                                return 'Ayah : '+d.nama_ayah + ' <br> Ibu : ' + d.nama_ibu;
                            }
                        },
                        {
                            data: "alamat"
                        },
                    ],
                    columnDefs: [{
                        targets: 7,
                        data: "id_siswa",
                        render: function(data) {
                            return '<div role="group" class="btn-group-xs btn-group btn-group-toggle" data-toggle="buttons">' +
                                '<button class="btn btn-warning edit" data-id="' + data +
                                '">Ubah</button>' +
                                '<button class="btn btn-danger hapus" data-id="' + data +
                                '">Hapus</button>' +
                                '</div>';
                        }
                    }]
                });
            }

            $.get(base_url + "data-kelas/tables",
                function(data, textStatus, jqXHR) {
                    $.each(data, function(index, value) {
                        kelas += '<option value="' + value.id_kelas + '">' + value.nama_kelas +
                            '</option>';
                    });
                    $('#kelas').append(kelas);
                },
                "json"
            );

            $.get(base_url + "data-angkatan/tables",
                function(data, textStatus, jqXHR) {
                    $.each(data, function(index, value) {
                        angkatan += '<option value="' + value.id_angkatan + '">' + value.nama_angkatan +
                            '</option>';
                    });
                    $('#angkatan').append(angkatan);
                },
                "json"
            );

            $('.tambah').on('click', function() {
                $('#add-modal').modal('show');
            });

            $('#dt-table').on('click', '.edit', function() {
                let id = $(this).data('id');
                let DataForm = $('#form-siswa');
                let name = [];
                $.ajax({
                    type: "get",
                    url: base_url + "data-siswa/tables",
                    data: {
                        id: id
                    },
                    dataType: "JSON",
                    success: function(response) {
                        $.each(response, function (indexInArray, val) { 
                            if(indexInArray == 'id_siswa'){
                                name['id_']=val;
                            }else{
                                name[indexInArray]=val;
                            }
                             
                        });
                        $('#add-modal').modal('show');
                        $.each(DataForm.serializeArray(), function(ind, value) {
                            $('[name="' + value.name + '"]').val(name[value.name]);
                        });
                    }
                });
            });

            $('#dt-table').on('click', '.hapus', function() {
                let id = $(this).data('id');
                Swal.fire({
                    title: 'Anda Yakin?',
                    text: "Data Akan Dihapus",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            type: "post",
                            url: base_url + "data-siswa/delete",
                            data: {
                                id: id
                            },
                            dataType: "JSON",
                            success: function(response) {
                                LoadTables();
                                if (response.success) {
                                    Swal.fire({
                                        position: 'center',
                                        icon: 'success',
                                        title: response.message,
                                        showConfirmButton: false,
                                        timer: 1500
                                    })
                                } else {
                                    Swal.fire({
                                        position: 'center',
                                        icon: 'success',
                                        title: response.message,
                                        showConfirmButton: false,
                                        timer: 1500
                                    })
                                }

                            },
                            error: function(request, textStatus, errorThrown) {
                                console.log(request.responseText);
                            }
                        });
                    }
                })

            });

            $('.refresh').on('click', function() {
                LoadTables();
            });

            $('.empty').on('click', function() {
                Swal.fire({
                    title: 'Anda Yakin?',
                    text: "Semua Data Akan Di Hapus",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            type: "get",
                            url: "data-siswa/empty-data",
                            dataType: "JSON",
                            success: function(response) {
                                LoadTables();
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    title: response.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                })
                            }
                        });
                    }
                })

            });


            let validator = $("#form-siswa").validate({
                rules: {
                    "nama_siswa": "required",
                    "nis": "required",
                    "nisn": "required",
                    "kelamin": "required",
                    "angkatan": "required",
                    "kelas": "required",
                    "nama_ayah": "required",
                    "nama_ibu": "required",
                    "alamat": "required",
                },
                messages: {
                    "nama_siswa": "tidak boleh kosong",
                    "nis": "tidak boleh kosong",
                    "nisn": "tidak boleh kosong",
                    "kelamin": "tidak boleh kosong",
                    "angkatan": "tidak boleh kosong",
                    "kelas": "tidak boleh kosong",
                    "nama_ayah": "tidak boleh kosong",
                    "nama_ibu": "tidak boleh kosong",
                    "alamat": "tidak boleh kosong",
                },
                errorElement: "div",
                errorPlacement: function(error, element) {
                    // Add the `invalid-feedback` class to the error element
                    error.addClass("invalid-feedback");
                    if (element.hasClass("select2")) {
                        error.insertAfter(element.next("span"));
                    } else {
                        error.insertAfter(element);
                    }
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-invalid").removeClass("is-valid");
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-valid").removeClass("is-invalid");
                },
                submitHandler: Submitsiswa,
            });

            function Submitsiswa() {
                let btn = $('.btn-save');
                let DataForm = $('#form-siswa');
                $.ajax({
                    type: "post",
                    url: base_url + "data-siswa/save",
                    data: DataForm.serialize(),
                    dataType: "JSON",
                    beforeSend: function() {
                        btn.html(
                            '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Prosessing'
                        );
                        btn.attr('disabled', true);
                    },
                    success: function(response) {
                        LoadTables();
                        DataForm[0].reset();
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: response.message,
                            showConfirmButton: false,
                            timer: 1500
                        })
                    },
                    complete: function() {
                        $.each(DataForm.serializeArray(), function(ind, value) {
                            $('[name="' + value.name + '"]').removeClass('is-valid');
                            $('[name="' + value.name + '"]').removeClass('is-invalid');
                        });
                        setTimeout(() => {
                            $('#add-modal').modal('hide');
                            btn.html('Submit');
                            btn.attr('disabled', false);
                        }, 1000);

                    },
                    error: function(request, textStatus, errorThrown) {
                        console.log(request.responseText);
                    }
                });
            }


        });
    </script>
@endsection
