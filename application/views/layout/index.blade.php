<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>{{ $title }} - SISTEM SPP</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="@asset('')assets/img/favicon.png" rel="icon">
    <link href="@asset('')assets/img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.gstatic.com" rel="preconnect">
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
        rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="@asset('')assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="@asset('')assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="@asset('')assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="@asset('')assets/vendor/quill/quill.snow.css" rel="stylesheet">
    <link href="@asset('')assets/vendor/quill/quill.bubble.css" rel="stylesheet">
    <link href="@asset('')assets/vendor/remixicon/remixicon.css" rel="stylesheet">
    {{-- <link href="@asset('')assets/vendor/simple-datatables/style.css" rel="stylesheet"> --}}
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@mdi/font@7.1.96/css/materialdesignicons.min.css">

    <!-- Template Main CSS File -->
    <link href="@asset('')assets/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="@asset('node_modules/')@sweetalert2/theme-bulma/bulma.css">
    {{-- css datatables --}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/dataTables.bootstrap5.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.4.0/css/responsive.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.3.2/css/buttons.dataTables.min.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.3.0/dist/select2-bootstrap-5-theme.min.css" />
    <script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM="
        crossorigin="anonymous"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/additional-methods.min.js"></script>
    <style>
        .btn-group-xs>.btn,
        .btn-xs {
            --bs-btn-padding-y: 0.25rem;
            --bs-btn-padding-x: 0.5rem;
            --bs-btn-font-size: 0.7rem;
            --bs-btn-border-radius: 0.25rem;
        }
        thead tr th{
            font-size: 12px;
        }

        tbody tr td{
            font-size: 12px;
        }
    </style>

</head>

<body>

    <!-- ======= Header ======= -->
    @include('layout.header')
    <!-- End Header -->

    <!-- ======= Sidebar ======= -->
    @include('layout.sidebar')
    <!-- End Sidebar-->

    <main id="main" class="main" style="margin-bottom:50px">

        <div class="pagetitle">
            <h1> {{ $title }} </h1>
            <nav>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                    <li class="breadcrumb-item active">{{ $title }}</li>
                </ol>
            </nav>
        </div><!-- End Page Title -->

        @yield('content')

    </main><!-- End #main -->

    <!-- ======= Footer ======= -->
    @include('layout.footer')
    <!-- End Footer -->

    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
            class="bi bi-arrow-up-short"></i></a>

    <!-- Vendor JS Files -->

    <script src="@asset('')assets/vendor/apexcharts/apexcharts.min.js"></script>
    <script src="@asset('')assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="@asset('')assets/vendor/chart.js/chart.min.js"></script>
    <script src="@asset('')assets/vendor/echarts/echarts.min.js"></script>
    <script src="@asset('')assets/vendor/quill/quill.min.js"></script>
    {{-- <script src="@asset('')assets/vendor/simple-datatables/simple-datatables.js"></script> --}}
    <script src="@asset('')assets/vendor/tinymce/tinymce.min.js"></script>
    <script src="@asset('')assets/vendor/php-email-form/validate.js"></script>
    <!-- Template Main JS File -->
    <script src="@asset('')assets/js/main.js"></script>
    <script src="@asset('node_modules/')sweetalert2/dist/sweetalert2.min.js"></script>
    {{-- js datatables --}}
    <script src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.1/js/dataTables.bootstrap5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.3.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.3.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.3.2/js/buttons.print.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.2/moment.min.js"></script>


    <script src="@asset('')assets/js/simple.money.format.js"></script>
    <script src="@asset('assets/')js/jquery.number.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.full.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.4.0/js/dataTables.responsive.min.js"></script>

    <script>
        const base_url = '{{ base_url() }}';

        $(function () {
            $.get("data-sekolah/detail",
                function (data, textStatus, jqXHR) {
                    $('#name-sekolah').text(data.nama_sekolah);
                    $('#logo-sekolah').attr('src',base_url+'assets/img/'+data.logo);
                },
                "json"
            );

            $.get("pengaturan/detail",
                function (data, textStatus, jqXHR) {
                    $('.header').css('background-color',data.bg_header);
                    $('#name-sekolah, .nav-heading').css('color',data.color_text);
                    $('#user-name').css('color',data.color_text);
                    $('.sidebar').css('background-color',data.bg_aside);
                },
                "json"
            );
        });
    </script>

</body>

</html>
