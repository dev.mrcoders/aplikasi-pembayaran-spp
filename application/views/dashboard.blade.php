@extends('layout.index')
@section('content')
    <section class="section dashboard">
        <div class="row">
            @if (is_user('level') == 1 || is_user('level') == 2)
                <!-- Left side columns -->
                <div class="col-lg-12">
                    <div class="row">

                        <!-- Sales Card -->
                        <div class="col-xxl-4 col-md-4">
                            <div class="card info-card revenue-card">
                                <div class="card-body">
                                    <h5 class="card-title">Total Pembayaran</span></h5>

                                    <div class="d-flex align-items-center">
                                        <div
                                            class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                            <i class="bi bi-currency-dollar"></i>
                                        </div>
                                        <div class="ps-3">
                                            <h6 id="ttl_bayar"></h6>


                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div><!-- End Sales Card -->

                        <!-- Revenue Card -->
                        <div class="col-xxl-2 col-md-3">
                            <div class="card info-card revenue-card">
                                <div class="card-body">
                                    <h5 class="card-title">Total Tagihan</h5>

                                    <div class="d-flex align-items-center">
                                        <div
                                            class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                            <i class="bi bi-receipt"></i>
                                        </div>
                                        <div class="ps-3">
                                            <h6 id="ttl_tagihan"></h6>


                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div><!-- End Revenue Card -->

                        <!-- Customers Card -->
                        <div class="col-xxl-3 col-md-2">

                            <div class="card info-card customers-card">


                                <div class="card-body">
                                    <h5 class="card-title">Total Kelas</h5>

                                    <div class="d-flex align-items-center">
                                        <div
                                            class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                            <i class="bi bi-person-lines-fill"></i>
                                        </div>
                                        <div class="ps-3">
                                            <h6 id="jml_kelas">1244</h6>


                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div><!-- End Customers Card -->

                        <div class="col-xxl-3 col-md-3">

                            <div class="card info-card customers-card">

                                <div class="card-body">
                                    <h5 class="card-title">Total Siswa</h5>

                                    <div class="d-flex align-items-center">
                                        <div
                                            class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                            <i class="bi bi-people"></i>
                                        </div>
                                        <div class="ps-3">
                                            <h6 id="jml_siswa"></h6>


                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>


                    </div>
                </div><!-- End Left side columns -->

                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Statistik Pembayaran</h5>

                            <!-- Line Chart -->
                            <canvas id="lineChart"
                                style="max-height: 400px; display: block; box-sizing: border-box; height: 221px; width: 442px;"
                                width="442" height="221"></canvas>

                            <!-- End Line CHart -->

                        </div>
                    </div>
                </div>
            @else
                <style>
                    .user-card-full {
                        overflow: hidden;
                    }

                    .card {
                        border-radius: 5px;
                        -webkit-box-shadow: 0 1px 20px 0 rgba(69, 90, 100, 0.08);
                        box-shadow: 0 1px 20px 0 rgba(69, 90, 100, 0.08);
                        border: none;
                        margin-bottom: 30px;
                    }

                    .m-r-0 {
                        margin-right: 0px;
                    }

                    .m-l-0 {
                        margin-left: 0px;
                    }

                    .user-card-full .user-profile {
                        border-radius: 5px 0 0 5px;
                    }

                    .bg-c-lite-green {
                        background: -webkit-gradient(linear, left top, right top, from(#f29263), to(#ee5a6f));
                        background: linear-gradient(to right, #ee5a6f, #f29263);
                    }

                    .user-profile {
                        padding: 20px 0;
                    }

                    .card-block {
                        padding: 1.25rem;
                    }

                    .m-b-25 {
                        margin-bottom: 25px;
                    }

                    .img-radius {
                        border-radius: 5px;
                    }



                    h6 {
                        font-size: 14px;
                    }

                    .card .card-block p {
                        line-height: 25px;
                    }

                    @media only screen and (min-width: 1400px) {
                        p {
                            font-size: 14px;
                        }
                    }

                    .card-block {
                        padding: 1.25rem;
                    }

                    .b-b-default {
                        border-bottom: 1px solid #e0e0e0;
                    }

                    .m-b-20 {
                        margin-bottom: 20px;
                    }

                    .p-b-5 {
                        padding-bottom: 5px !important;
                    }

                    .card .card-block p {
                        line-height: 25px;
                    }

                    .m-b-10 {
                        margin-bottom: 10px;
                    }

                    .text-muted {
                        color: #919aa3 !important;
                    }

                    .b-b-default {
                        border-bottom: 1px solid #e0e0e0;
                    }

                    .f-w-600 {
                        font-weight: 600;
                    }

                    .m-b-20 {
                        margin-bottom: 20px;
                    }

                    .m-t-40 {
                        margin-top: 20px;
                    }

                    .p-b-5 {
                        padding-bottom: 5px !important;
                    }

                    .m-b-10 {
                        margin-bottom: 10px;
                    }

                    .m-t-40 {
                        margin-top: 20px;
                    }

                    .user-card-full .social-link li {
                        display: inline-block;
                    }

                    .user-card-full .social-link li a {
                        font-size: 20px;
                        margin: 0 10px 0 0;
                        -webkit-transition: all 0.3s ease-in-out;
                        transition: all 0.3s ease-in-out;
                    }
                </style>

                <div class="col-xl-12 col-md-12">
                    <div class="card user-card-full">
                        <div class="row m-l-0 m-r-0">
                            <div class="col-sm-4 bg-c-lite-green user-profile">
                                <div class="card-block text-center text-white">
                                    <div class="m-b-25">
                                        <img src="https://img.icons8.com/bubbles/100/000000/user.png" class="img-radius"
                                            alt="User-Profile-Image">
                                    </div>
                                    <h4 class="f-w-600">{{ is_siswa(is_user('username'))->nama_siswa }}</h4>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="card-block">
                                    <h6 class="m-b-20 p-b-5 b-b-default f-w-600">Information</h6>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <p class="m-b-10 f-w-600">NIS</p>
                                            <h6 class="text-muted f-w-400">{{ is_siswa(is_user('username'))->nis }}</h6>
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="m-b-10 f-w-600">NISN</p>
                                            <h6 class="text-muted f-w-400">{{ is_siswa(is_user('username'))->nisn }}</h6>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <p class="m-b-10 f-w-600">Kelamin</p>
                                            <h6 class="text-muted f-w-400">
                                                {{ is_siswa(is_user('username'))->kelamin == 'L' ? 'Laki - laki' : 'Perempuan' }}
                                            </h6>
                                        </div>
                                        <div class="col-sm-4">
                                            <p class="m-b-10 f-w-600">Kelas</p>
                                            <h6 class="text-muted f-w-400">{{ is_siswa(is_user('username'))->nama_kelas }}
                                            </h6>
                                        </div>
                                        <div class="col-sm-4">
                                            <p class="m-b-10 f-w-600">Angkatan</p>
                                            <h6 class="text-muted f-w-400">
                                                {{ is_siswa(is_user('username'))->nama_angkatan }}</h6>
                                        </div>
                                        <div class="col-sm-12">
                                            <p class="m-b-10 f-w-600">Alamat</p>
                                            <h6 class="text-muted f-w-400">{{ is_siswa(is_user('username'))->alamat }}</h6>
                                        </div>
                                    </div>
                                    <h6 class="m-b-20 m-t-40 p-b-5 b-b-default f-w-600">Wali Murid/Orang Tua</h6>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <p class="m-b-10 f-w-600">Ayah</p>
                                            <h6 class="text-muted f-w-400">{{ is_siswa(is_user('username'))->nama_ayah }}
                                            </h6>
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="m-b-10 f-w-600">Ibu</p>
                                            <h6 class="text-muted f-w-400">{{ is_siswa(is_user('username'))->nama_ibu }}
                                            </h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif


        </div>
    </section>
    <script>
        document.addEventListener("DOMContentLoaded", () => {
            let dta;
            $.get(base_url + "dashboard/stats",
                function(data, textStatus, jqXHR) {

                    $('#ttl_tagihan').text(data.data.JmlTagihan.ttl_)
                    $('#jml_kelas').text(data.data.JmlKelas.ttl_)
                    $('#jml_siswa').text(data.data.JmlSiswa.ttl_)
                    $('#ttl_bayar').text('Rp ' + $.number(parseInt(data.data.ttlBayar.ttl_), 0, ',','.'))
                    new Chart(document.querySelector('#lineChart'), {
                        type: 'line',
                        data: {
                            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July',
                                'Agusturs',
                                'September', 'Oktober', 'November', 'Desember'
                            ],
                            datasets: [{
                                label: 'Pembayaran',
                                data: data.data.bulan,
                                fill: true,
                                borderColor: 'rgb(75, 192, 192)',
                                tension: 0.1
                            }]
                        },
                        options: {
                            scales: {
                                y: {
                                    beginAtZero: true
                                }
                            }
                        }
                    });

                },
                "JSON"
            );


        });
    </script>
@endsection
