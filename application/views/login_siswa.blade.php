<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Login - SISTEM SPP</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="@asset('')assets/img/favicon.png" rel="icon">
    <link href="@asset('')assets/img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.gstatic.com" rel="preconnect">
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
        rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="@asset('')assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="@asset('')assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="@asset('')assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="@asset('')assets/vendor/quill/quill.snow.css" rel="stylesheet">
    <link href="@asset('')assets/vendor/quill/quill.bubble.css" rel="stylesheet">
    <link href="@asset('')assets/vendor/remixicon/remixicon.css" rel="stylesheet">
    <link href="@asset('')assets/vendor/simple-datatables/style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@mdi/font@7.1.96/css/materialdesignicons.min.css">

    <!-- Template Main CSS File -->
    <link href="@asset('')assets/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="@asset('node_modules/')@sweetalert2/theme-bulma/bulma.css">

</head>

<body>

    <main>
        <div class="container">

            <section
                class="section register min-vh-100 d-flex flex-column align-items-center justify-content-center py-4">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-4 col-md-6 d-flex flex-column align-items-center justify-content-center">

                            <div class="d-flex justify-content-center py-4">
                                <a href="index.html" class="logo d-flex align-items-center w-auto">
                                    <img src="assets/img/logo.png" alt="">
                                    <span class="d-none d-lg-block">SISTEM SPP</span>
                                </a>
                            </div><!-- End Logo -->

                            <div class="card mb-3">

                                <div class="card-body">

                                    <div class="pt-4 pb-2">
                                        <h5 class="card-title text-center pb-0 fs-4">Login to Your Account</h5>
                                        <p class="text-center small">Enter your username & password to login</p>
                                    </div>

                                    <form class="row g-3 needs-validation" novalidate id="form-login">

                                        <div class="col-12">
                                            <label for="yourUsername" class="form-label">NIS (Nomor Induk Siswa)</label>
                                            <div class="input-group has-validation">
                                                <span class="input-group-text" id="inputGroupPrepend"><i
                                                        class="mdi mdi-account"></i></span>
                                                <input type="text" name="username" class="form-control"
                                                    id="yourUsername" required>
                                                <div class="invalid-feedback">Please enter your username.</div>
                                            </div>
                                        </div>

                                        <div class="col-12">
                                            <label for="yourPassword" class="form-label">Password</label>
                                            <div class="input-group has-validation">
                                                <span class="input-group-text" id="inputGroupPrepend"><i
                                                        class="mdi mdi-account-key"></i></span>
                                                <input type="password" name="password" class="form-control"
                                                    id="password" required>
                                                <div class="invalid-feedback">Please enter your password.</div>
                                            </div>
                                        </div>

                                        <div class="col-12">
                                            <button class="btn btn-primary w-100" type="submit" id="btn-login">
                                                Login
                                            </button>
                                        </div>
                                    </form>

                                </div>
                            </div>


                        </div>
                    </div>
                </div>

            </section>

        </div>
    </main><!-- End #main -->

    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
            class="bi bi-arrow-up-short"></i></a>

    <!-- Vendor JS Files -->
    <script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM="
        crossorigin="anonymous"></script>
    <script src="@asset('')assets/vendor/apexcharts/apexcharts.min.js"></script>
    <script src="@asset('')assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="@asset('')assets/vendor/chart.js/chart.min.js"></script>
    <script src="@asset('')assets/vendor/echarts/echarts.min.js"></script>
    <script src="@asset('')assets/vendor/quill/quill.min.js"></script>
    <script src="@asset('')assets/vendor/simple-datatables/simple-datatables.js"></script>
    <script src="@asset('')assets/vendor/tinymce/tinymce.min.js"></script>
    <script src="@asset('')assets/vendor/php-email-form/validate.js"></script>
    <!-- Template Main JS File -->
    <script src="@asset('')assets/js/main.js"></script>
    <script src="@asset('node_modules/')sweetalert2/dist/sweetalert2.min.js"></script>

    <script>
        const base_url = '{{ base_url() }}';
    </script>
    <script>
        $(function() {

            $('#form-login').submit(function(e) {
                e.preventDefault();
                e.stopImmediatePropagation();
                let btn = $('#btn-login');
                let DataForm = $(this).serialize();
                $.ajax({
                    type: "POST",
                    url: base_url + "login-siswa",
                    data: DataForm,
                    dataType: "JSON",
                    beforeSend: function() {
                        btn.html(
                            '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Prosessing'
                        );
                        btn.attr('disabled', true);
                    },
                    success: function(response) {
                        if (response.success) {
                            window.location.href=base_url + response.data.redirect
                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: response.message,
                            })
                        }
                    },
                    complete: function() {
                        setTimeout(() => {
                            btn.html('LogIn');
                            btn.attr('disabled', false);
                        }, 1000);

                    }
                });

            });
        });
    </script>

</body>

</html>
