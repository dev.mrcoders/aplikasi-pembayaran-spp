@extends('layout.index')
@section('content')
    <section class="section">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row pt-3">
                            <div class="col-sm-8">
                                <div class="card border border-primary">
                                    <div class="card-header">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                Tabel Data Angkatan
                                            </div>
                                            <div class="col-sm-8 text-end">
                                                <div class="btn-group btn-group-xs ">
                                                    <button class="btn btn-secondary refresh">Refresh</button>
                                                    <button class="btn btn-info empty" hidden>Kosongkan Data</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body m-3">
                                        <table class="table table-sm" id="dt-table">
                                            <thead>
                                                <tr>
                                                    <th scope="col">#</th>
                                                    <th scope="col">Nama Angkatan</th>
                                                    <th scope="col">Tahun</th>
                                                    <th scope="col">semester</th>
                                                    <th scope="col">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                            <div class="col-sm-4">
                                <div class="card  border border-primary">
                                    <div class="card-header">
                                        Form Data angkatan
                                    </div>
                                    <div class="card-body m-3">
                                        <form class="row g-3" id="form-angkatan">
                                            <input type="hidden" name="id_" id="id_" value="">
                                            <div class="col-12">
                                                <label for="inputNanme4" class="form-label">Nama Angkatan</label>
                                                <input type="text" class="form-control" id="angkatan" name="angkatan"
                                                    required>
                                            </div>
                                            <div class="col-12">
                                                <label for="inputNanme4" class="form-label">Tahun</label>
                                                <input type="text" class="form-control" id="tahun" name="tahun"
                                                    required>
                                            </div>
                                            <div class="col-12">
                                                <label for="inputNanme4" class="form-label">semester</label>
                                                <input type="text" class="form-control" id="semester" name="semester"
                                                    required>
                                            </div>

                                            <div class="text-center">
                                                <button type="submit"
                                                    class="btn btn-primary btn-xs btn-save">Submit</button>
                                                <button type="reset" class="btn btn-secondary btn-xs">Reset</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </section>
    <script>
        $(function() {
            LoadTables();

            function LoadTables() {
                $('#dt-table').DataTable({
                    stateSave: true,
                    destroy: true,
                    ajax: {
                        url: base_url + "data-angkatan/tables",
                        type: "POST",
                        dataSrc: "",
                    },
                    columns: [{
                            render: function(data, type, row, meta) {
                                return meta.row + 1;
                            }
                        },
                        {
                            data: "nama_angkatan"
                        },
                        {
                            data: "tahun"
                        },
                        {
                            data: "semester"
                        },
                    ],
                    columnDefs: [{
                        targets: 4,
                        data: "id_angkatan",
                        render: function(data) {
                            return '<div role="group" class="btn-group-xs btn-group btn-group-toggle" data-toggle="buttons">' +
                                '<button class="btn btn-warning edit" data-id="' + data +
                                '">Ubah</button>' +
                                '<button class="btn btn-danger hapus" data-id="' + data +
                                '">Hapus</button>' +
                                '</div>';
                        }
                    }]
                });
            }

            $('#dt-table').on('click', '.edit', function() {
                let id = $(this).data('id');
                $.ajax({
                    type: "get",
                    url: base_url + "data-angkatan/tables",
                    data: {
                        id: id
                    },
                    dataType: "JSON",
                    success: function(response) {
                        $('#id_').val(id);
                        $('#angkatan').val(response.nama_angkatan);
                        $('#tahun').val(response.tahun);
                        $('#semester').val(response.semester);
                    }
                });
            });

            $('#dt-table').on('click', '.hapus', function() {
                let id = $(this).data('id');
                Swal.fire({
                    title: 'Anda Yakin?',
                    text: "Data Akan Dihapus",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            type: "post",
                            url: base_url + "data-angkatan/delete",
                            data: {
                                id: id
                            },
                            dataType: "JSON",
                            success: function(response) {
                                LoadTables();
                                if (response.success) {
                                    Swal.fire({
                                        position: 'center',
                                        icon: 'success',
                                        title: response.message,
                                        showConfirmButton: false,
                                        timer: 1500
                                    })
                                } else {
                                    Swal.fire({
                                        position: 'center',
                                        icon: 'success',
                                        title: response.message,
                                        showConfirmButton: false,
                                        timer: 1500
                                    })
                                }

                            },
                            error: function(request, textStatus, errorThrown) {
                                console.log(request.responseText);
                            }
                        });
                    }
                })

            });

            $('.refresh').on('click', function() {
                LoadTables();
            });

            $('.empty').on('click', function() {
                Swal.fire({
                    title: 'Anda Yakin?',
                    text: "Semua Data Akan Di Hapus",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            type: "get",
                            url: "data-angkatan/empty-data",
                            dataType: "JSON",
                            success: function(response) {
                                LoadTables();
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    title: response.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                })
                            }
                        });
                    }
                })

            });


            let validator = $("#form-angkatan").validate({
                rules: {
                    "angkatan": "required",
                    "tahun": "required",
                    "semester": "required",
                },
                messages: {
                    "angkatan": "Tidak Boleh Kosong",
                    "tahun": "Tidak Boleh Kosong",
                    "semester": "Tidak Boleh Kosong",
                },
                errorElement: "div",
                errorPlacement: function(error, element) {
                    // Add the `invalid-feedback` class to the error element
                    error.addClass("invalid-feedback");
                    if (element.hasClass("select2")) {
                        error.insertAfter(element.next("span"));
                    } else {
                        error.insertAfter(element);
                    }
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-invalid").removeClass("is-valid");
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-valid").removeClass("is-invalid");
                },
                submitHandler: Submitangkatan,
            });

            function Submitangkatan() {
                let btn = $('.btn-save');
                let DataForm = $('#form-angkatan').serialize();

                $.ajax({
                    type: "post",
                    url: base_url + "data-angkatan/save",
                    data: DataForm,
                    dataType: "JSON",
                    success: function(response) {
                        LoadTables();
                        $('#id_').val('');
                        $('#angkatan, #tahun, #semester').val('');
                        $('#angkatan, #tahun, #semester').removeClass('is-valid');
                        $('#angkatan, #tahun, #semester').removeClass('is-invalid');
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: response.message,
                            showConfirmButton: false,
                            timer: 1500
                        })
                    },
                    error: function(request, textStatus, errorThrown) {
                        console.log(request.responseText);
                    }
                });
            }

        });
    </script>
@endsection
