<?php


/**
 * Some important function are here below!
 * Remove comment to start using any of these.
 */

if (!function_exists('is_user')) {

    /**
     * If the method of request is POST return true else false.
     * 
     *  @return bool  */
    function is_user($item)
    {
        $CI = &get_instance();
        return $CI->session->userdata($item);
    }
}

if (!function_exists('is_siswa')) {

    /**
     * If the method of request is POST return true else false.
     * 
     *  @return bool  */
    function is_siswa($item)
    {
        $CI = &get_instance();
        $CI->db->join('tb_angkatan b', 'b.id_angkatan = a.id_angkatan', 'inner');
        $CI->db->join('tb_kelas c', 'c.id_kelas = a.id_kelas', 'inner');
        $GetSiswa = $CI->db->get_where('tb_siswa a', ['a.nis' => $item]);

        return $GetSiswa->row();
    }
}

if (!function_exists('flashdata')) {

    function flashdata($type)
    {
        $CI = &get_instance();
        return $CI->session->flashdata($type);
    }
}




/* End of file view_helper.php and path \application\helpers\view_helper.php */
