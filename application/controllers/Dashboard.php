<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

    protected $data;
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->data['title']='Dashboard';
        $this->view('dashboard',$this->data);
    }

    public function Statistik()
    {
        $TotalBayar = $this->db->select('sum(jumlah_pembayaran)as ttl_')->get('tb_pembayaran')->row();
        $JumlahTagihan = $this->db->select('count(id_tagihan)as ttl_')->get('tb_tagihan')->row();

        $JmlKelas = $this->db->select('count(id_kelas)as ttl_')->get('tb_kelas')->row();
        $JmlSiswa = $this->db->select('count(id_siswa)as ttl_')->get('tb_siswa')->row();

        $Month=[];

        for($a = 1;$a <=12 ;$a++){
            $mnth = sprintf("%02d", $a);
            $this->db->select('sum(jumlah_pembayaran)as ttl');
            $this->db->where('SUBSTRING(tgl_bayar,1,4)',date('Y'));
            $this->db->where('SUBSTRING(tgl_bayar,6,2)',$mnth);
            $GetData = $this->db->get('tb_pembayaran')->row()->ttl;
            array_push($Month,($GetData != null ? $GetData: 0));
        }

        $Response=[
            'success'=>true,
            'data'=>[
                'ttlBayar'=>$TotalBayar,
                'JmlTagihan'=>$JumlahTagihan,
                'JmlKelas'=>$JmlKelas,
                'JmlSiswa'=>$JmlSiswa,
                'bulan'=>$Month
            ]
        ];
        $this->output->set_content_type('application/json')->set_output(json_encode($Response));
        
    }
}

/* End of file Dashboard.php and path \application\controllers\Dashboard.php */
