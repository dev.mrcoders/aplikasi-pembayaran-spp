<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Profile extends MY_Controller
{

    protected $data;
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->data['profile'] = $this->db->get('tb_profile_sekolah')->row();
        $this->data['title'] = 'Profile Sekolah';
        $this->view('profile.index', $this->data);
        // $this->output->set_content_type('application/json')->set_output(json_encode($this->data));
    }

    public function Save()
    {
        $this->db->select('id_sekolah,logo');
        $GetLogo = $this->db->get('tb_profile_sekolah', 1)->row();

        $config['upload_path'] = './assets/img/';
        $config['allowed_types'] = 'jpeg|jpg|png';

        $this->load->library('upload', $config);
        if (!empty($_FILES['logo']['name'])) {
            if (!$this->upload->do_upload('logo')) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('error', $error);
                redirect(base_url() . 'data-sekolah');
            } else {
                $data = $this->upload->data();
                unlink(base_url() . 'assets/img/' . $GetLogo->logo);
                $DataPost = [
                    'nama_sekolah' => $this->input->post('nama_sekolah'),
                    'alamat' => $this->input->post('alamat'),
                    'website' => $this->input->post('website'),
                    'email' => $this->input->post('email'),
                    'telepon' => $this->input->post('telepon'),
                    'pimpinan' => $this->input->post('pimpinan'),
                    'kepsek_mts' => $this->input->post('kepsek_mts'),
                    'kepsek_ma' => $this->input->post('kepsek_ma'),
                    'logo' => $data['file_name'],
                    'deskripsi' => $this->input->post('deskripsi')
                ];
                $this->db->where('id_sekolah', $GetLogo->id_sekolah);
                $this->db->update('tb_profile_sekolah', $DataPost);
                $this->session->set_flashdata('success', 'data sekolah berhasil di perbaharui');
                redirect(base_url() . 'data-sekolah');
            }
        } else {
            $DataPost = [
                'nama_sekolah' => $this->input->post('nama_sekolah'),
                'alamat' => $this->input->post('alamat'),
                'website' => $this->input->post('website'),
                'email' => $this->input->post('email'),
                'telepon' => $this->input->post('telepon'),
                'pimpinan' => $this->input->post('pimpinan'),
                'kepsek_mts' => $this->input->post('kepsek_mts'),
                'kepsek_ma' => $this->input->post('kepsek_ma'),
                'deskripsi' => $this->input->post('deskripsi')
            ];
            $this->db->where('id_sekolah', $GetLogo->id_sekolah);
            $this->db->update('tb_profile_sekolah', $DataPost);
            $this->session->set_flashdata('success', 'data sekolah berhasil di perbaharui');
            redirect(base_url() . 'data-sekolah');
        }
    }

    public function DetailSekolah()
    {
        $GetData = $this->db->get('tb_profile_sekolah',1)->row();

        $this->output->set_content_type('application/json')->set_output(json_encode($GetData));
        
    }

    public function detailsistem()
    {
        $GetData = $this->db->get('tb_pengaturan',1)->row();

        $this->output->set_content_type('application/json')->set_output(json_encode($GetData));
        
    }

    public function Pengaturan()
    {
        $this->data['setting'] = $this->db->get('tb_pengaturan',1)->row();
        $this->data['title'] = 'Pengaturan Sistem';
        $this->view('profile.pengaturan', $this->data);
    }

    public function SavePengaturan()
    {
        $DataPost=[
            'bg_header'=>$this->input->post('bg_header'),
            'color_text'=>$this->input->post('color_text'),
            'bg_aside'=>$this->input->post('bg_aside'),
        ];

        $this->db->where('id_pengaturan', $this->input->post('id'));
        $this->db->update('tb_pengaturan', $DataPost);

        $this->session->set_flashdata('success', 'Pengaturan Sistem Berhasil Di update');
        redirect(base_url() . 'pengaturan');
        
    }
}

/* End of file Profile.php and path \application\controllers\Profile.php */