<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Angkatan extends MY_Controller {

    protected $data;
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->data['title']='Data angkatan';
        $this->view('angkatan.index',$this->data);
        // $this->output->set_content_type('application/json')->set_output(json_encode($this->data));
    }

    public function Data()
    {
        if($this->input->get('id')){
            $this->db->where('id_angkatan', $this->input->get('id'));
        }
        $list = $this->db->get('tb_angkatan');

        $result = ($this->input->get('id') ? $list->row():$list->result() );

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
        
    }

    public function save()
    {
        $DataForm = [
            'nama_angkatan'=>$this->input->post('angkatan'),
            'tahun'=>$this->input->post('tahun'),
            'semester'=>$this->input->post('semester'),
        ];

        if(empty($this->input->post('id_'))){
            $this->db->insert('tb_angkatan', $DataForm);

            $Response =[
                'success'=>true,
                'status'=>"OK",
                'message'=>"Data angkatan Berhasil Di Tambahkan",
                'data'=>$DataForm
            ];
        }else{
            $this->db->where('id_angkatan', $this->input->post('id_'));
            $this->db->update('tb_angkatan', $DataForm);
            
            $Response =[
                'success'=>true,
                'status'=>"OK",
                'message'=>"Data angkatan Berhasil Di Ubah",
                'data'=>$DataForm
            ];
        }
        

        $this->output->set_content_type('application/json')->set_output(json_encode($Response));
        
    }

    public function delete()
    {
        $RelasiData = $this->db->get_where('tb_siswa', ['id_angkatan'=>$this->input->post('id')])->row();

        if($RelasiData){
            $Response =[
                'success'=>false,
                'status'=>"Gagal",
                'message'=>"Data angkatan ". $RelasiData->nama_angkatan . "Tidak dapat dihapus, sedang digunakan",
                'data'=>''
            ];
        }else{
            $this->db->where('id_angkatan', $this->input->post('id'));
            $this->db->delete('tb_angkatan');
            
            $Response =[
                'success'=>true,
                'status'=>"OK",
                'message'=>"Data angkatan Berhasil Di hapus",
                'data'=>''
            ];
        }
        
        $this->output->set_content_type('application/json')->set_output(json_encode($Response));
        
    }

    public function emptydata(Type $var = null)
    {
        $this->db->empty_table('tb_angkatan');
        $Response =[
            'success'=>true,
            'status'=>"OK",
            'message'=>"Data Berhasil Di kosongkan",
            'data'=>''
        ];

        $this->output->set_content_type('application/json')->set_output(json_encode($Response));
        
    }
}

/* End of file Angkatan.php and path \application\controllers\Angkatan.php */