<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayaran extends MY_Controller {

    protected $data;
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->data['title']='Data Pembayaran';
        $this->view('bills.pembayaran',$this->data);
        // $this->output->set_content_type('application/json')->set_output(json_encode($this->data));
    }

    public function rekap()
    {
        if($this->session->userdata('level') != 0){
            $this->data['title']='Data Rekap Pembayaran';
        }else{
            $this->data['title']='Data Histori Pembayaran';
        }
        
        $this->view('bills.rekap',$this->data);
    }

    public function Data()
    {
        $id_siswa = $this->input->post('siswa');
        $id_tagihan = $this->input->post('tagihan');
        
        $tagihan = $this->db->get_where('tb_tagihan',['id_tagihan'=>$id_tagihan])->result();
        $data=array();
        foreach($tagihan as $list){
            $Pembayaran = $this->db->get_where('tb_pembayaran', ['id_siswa'=>$id_siswa,'id_tagihan'=>$id_tagihan]);
            // if($Pembayaran->num_rows() > 0){
            //     $this->db->order_by('id_pembayaran', 'desc');
            //     $status = $this->db->get_where('tb_pembayaran', ['id_siswa'=>$id_siswa,'id_tagihan'=>$id_tagihan],1)->row()->status;
                
            // }else{
                // $status = 0;
            //     $sisa = 0;
            // }
            $total_for_status =[];
            foreach($Pembayaran->result() as $pays){
                $total_for_status[]=$pays->jumlah_pembayaran;   
            }
            if(array_sum($total_for_status) == $list->total_tagihan){
                $status = 1;
            }else{
                $status =0;
            }
            $row=array();
            $row['id_siswa']=$id_siswa;
            $row['id_tagihan']=$list->id_tagihan;
            $row['tagihan']=$list->jenis_tagihan;
            $row['nominal']=$list->total_tagihan;
            $row['stats']=$status;
            $row['status']=($status == 0 ? '<span class="badge bg-danger">Tunggak</span>':'<span class="badge bg-success">Lunas</span>');
            $data2=array();
            foreach($Pembayaran->result() as $pay){
                $row2=array();
                $row2['id_pembayaran']=$pay->id_pembayaran;
                $row2['tgl_pembayaran']=$pay->tgl_bayar;
                $row2['di_bayar']=$pay->jumlah_pembayaran;
                $row2['bulan']=$pay->bln_thn_bayar;
                $row2['operator']=$pay->user_created;
                $data2[]=$row2;
            }
            $row['rincian']=$data2;
            $data[]=$row;
        }
        $output = array(
         "draw" => $this->input->post('draw'),
         "recordsTotal" => count($tagihan),
         "recordsFiltered" => count($tagihan),
         "data" => $data,
     );
        $this->output->set_content_type('application/json')->set_output(json_encode($output));
        
    }

    public function Save()
    {
        $dibayar = explode(',',$this->input->post('jumlah_bayar'));
        $this->db->select_sum('jumlah_pembayaran');
        $GetDataBayar = $this->db->get_where('tb_pembayaran', ['id_tagihan'=>$this->input->post('id_tagihan'),'id_siswa'=>$this->input->post('id_siswa')])->row();
        $GetTagihan = $this->db->get_where('tb_tagihan', ['id_tagihan'=>$this->input->post('id_tagihan')])->row();
        $sisa = $GetTagihan->total_tagihan - join($dibayar) - $GetDataBayar->jumlah_pembayaran;
        $status = ($sisa == 0 ? 1:0);

        $DataForm=[
            'id_tagihan'=>$this->input->post('id_tagihan'),
            'id_siswa'=>$this->input->post('id_siswa'),
            'tgl_bayar'=>date('Y-m-d H:i:s'),
            'bln_thn_bayar'=>$this->input->post('tahun').'-'.$this->input->post('bulan'),
            'jumlah_pembayaran'=>join($dibayar),
            'sisa'=>$sisa,
            'status'=>$status,
            'user_created'=>$this->session->userdata('username')
            
        ];

        $this->db->insert('tb_pembayaran', $DataForm);
        
        $Response =[
            'success'=>true,
            'status'=>"OK",
            'message'=>"Pembayayan Berhasil Di Lakukan",
            'data'=>$DataForm
        ];

        $this->output->set_content_type('application/json')->set_output(json_encode($Response));
        
    }

    public function Delete()
    {
        $ValidasiPassword = $this->db->get_where('tb_user', ['password'=>md5($this->input->post('password'))])->row();
        if($ValidasiPassword){
            $this->db->where('id_pembayaran', $this->input->post('id'));
            $this->db->delete('tb_pembayaran');
            
            $Response =[
                'success'=>true,
                'status'=>"OK",
                'message'=>"Data Transaksi Pembayaran berhasil Dihapus",
                'data'=>[]
            ];
        }else{
            $Response =[
                'success'=>false,
                'status'=>"ERROR",
                'message'=>"Password Salah",
                'data'=>[]
            ];
        }
        
        $this->output->set_content_type('application/json')->set_output(json_encode($Response));
        
    }

    public function DataRekap()
    {

        $this->db->select('a.*,b.jenis_tagihan,c.nama_siswa,c.nis,c.nisn');
        $this->db->from('tb_pembayaran a');
        $this->db->join('tb_tagihan b','a.id_tagihan=b.id_tagihan','inner');
        $this->db->join('tb_siswa c','a.id_siswa=c.id_siswa','inner');
        if($this->input->post('id_siswa')){
            $this->db->where('c.id_siswa', $this->input->post('id_siswa'));
        }
        if($this->input->post('id_tagihan')){
           $this->db->where('b.id_tagihan', $this->input->post('id_tagihan')); 
        }
        $list =  $this->db->get();

        $this->output->set_content_type('application/json')->set_output(json_encode($list->result()));
        
    }

    public function DataStatistik(Type $var = null)
    {
        $JmlSiswa = $this->db->select('count(id_siswa) as jml')->get('tb_siswa')->row();
        $DataTagihan = $this->db->get('tb_tagihan');
        $Total=[];
        foreach($DataTagihan->result() as $dt){
            $Ttl=[];
            $Ttl['total']=$dt->total_tagihan * $JmlSiswa->jml;
            $Total[]=$Ttl;
        }

        $Totaltagihan = array_sum(array_column($Total, 'total'));

        $ChartBar = $this->db->select('b.jenis_tagihan,SUM(a.jumlah_pembayaran)as ttl')->from('tb_pembayaran a')->join('tb_tagihan b','b.id_tagihan=a.id_tagihan','inner')->group_by('a.id_tagihan')->get()->result();

        // $Totaltagihan = $this->db->select('sum(total_tagihan)as ttl_')->where('active','Y')->get('tb_tagihan')->row();
        $TotalBayar = $this->db->select('sum(jumlah_pembayaran)as ttl_')->get('tb_pembayaran')->row();
        $TotalDays = $this->db->select('sum(jumlah_pembayaran)as ttl_')
        ->where('SUBSTRING(tgl_bayar,1,10)',date('Y-m-d'))
        ->get('tb_pembayaran')
        ->row();
        $TotalMonth = $this->db->select('sum(jumlah_pembayaran)as ttl_')->where('SUBSTRING(tgl_bayar,6,2)',date('m'))->get('tb_pembayaran')->row();
        $TotalYears = $this->db->select('sum(jumlah_pembayaran)as ttl_')->where('SUBSTRING(tgl_bayar,1,4)',date('Y'))->get('tb_pembayaran')->row();
        $Response=[
            'success'=>true,
            'data'=>[
                'Chart'=>$ChartBar,
                'ttl_tagihan'=>$Totaltagihan,
                'ttl_bayar'=>$TotalBayar->ttl_,
                'thisDays'=>$TotalDays->ttl_,
                'thisMonth'=>$TotalMonth->ttl_,
                'thisYears'=>$TotalYears->ttl_,
            ]
        ];
        $this->output->set_content_type('application/json')->set_output(json_encode($Response));
        
    }

    public function statistikjumlahbayar()
    {
        $datas=[];
        if($this->input->get('id')){
            $this->db->where('id_tagihan', $this->input->get('id'));
        }
        
        $tagihan = $this->db->get('tb_tagihan');
        foreach($tagihan->result() as $tagihans){
            $bayars=[];
            $notpays=[];
            $siswa = $this->db->get('tb_siswa')->result();
            foreach($siswa as $siswas){
                $Pembayaran = $this->db->get_where('tb_pembayaran', ['id_tagihan'=>$tagihans->id_tagihan,'id_siswa'=>$siswas->id_siswa]);
                $ttl=[];
                foreach($Pembayaran->result() as $pays){
                    $ttl[]=$pays->jumlah_pembayaran;
                }
                if(array_sum($ttl) == $tagihans->total_tagihan){
                    $bayars[$siswas->nis]= array_sum($ttl);
                }else{
                    $notpays[$siswas->nis]= array_sum($ttl);

                }

            }
            $datas[$tagihans->jenis_tagihan]=[
                'pays'=>count($bayars),
                'nopays'=>count($notpays),
            ];
            

            $data[]=$tagihans->jenis_tagihan;

        }
        $lunas=[];
        foreach($datas as $d){
            $lunas[] = $d['pays'];
            $tunggak[] = $d['nopays'];
        }
        $sm[]= array_sum($lunas);
        $sm[]= array_sum($tunggak);

        $this->output->set_content_type('application/json')->set_output(json_encode(['data'=>$datas,'tagihan'=>$data,'status'=>$sm]));
        
        
    }

    function weekOfMonth($date) {
        $firstOfMonth = date("Y-m-01", strtotime($date));
        return intval(date("W", strtotime($date))) - intval(date("W", strtotime($firstOfMonth)));
    }

    public function StatistikBysiswa()
    {
        $idsiswa=$this->input->get('siswa');
        $Details=[];
        $GetDataTagihan = $this->db->select('sum(total_tagihan)as jml')
        ->from('tb_tagihan')
        ->where('active','Y')
        ->get()
        ->row();

        $GetJumlahBayar=$this->db->select('sum(jumlah_pembayaran)as jml')
        ->from('tb_pembayaran')
        ->where('id_siswa',$idsiswa)
        ->get()
        ->row();

        if($GetDataTagihan){
            $Details['jml_tagihan']=$GetDataTagihan->jml;
            $Details['ttl_bayar']=$GetJumlahBayar->jml;
            $Details['sisa_tagihan']= $GetDataTagihan->jml - $GetJumlahBayar->jml;
        }

        $this->output->set_content_type('application/json')->set_output(json_encode($Details));
    }
}

/* End of file Pembayaran.php and path \application\controllers\Pembayaran.php */