<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa extends MY_Controller {

    protected $data;
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->data['title']='Data siswa';
        $this->view('siswa.index',$this->data);
        // $this->output->set_content_type('application/json')->set_output(json_encode($this->data));
    }

    public function Data()
    {
        $this->db->select('tb_siswa.*,tb_angkatan.nama_angkatan,tb_kelas.nama_kelas');
        
        if($this->input->get('id')){
            $this->db->where('id_siswa', $this->input->get('id'));
        }
        if($this->input->get('search')){
            $this->db->like('nama_siswa',$this->input->get('search'),'BOTH');
            $this->db->or_like('nis',$this->input->get('search'),'BOTH');
            $this->db->or_like('nisn',$this->input->get('search'),'BOTH');
        }
        $this->db->join('tb_kelas', 'tb_kelas.id_kelas = tb_siswa.id_kelas', 'inner');
        $this->db->join('tb_angkatan', 'tb_angkatan.id_angkatan = tb_siswa.id_angkatan', 'inner');
        
        $list = $this->db->get('tb_siswa');

        $result = ($this->input->get('id') ? $list->row():$list->result() );

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
        
    }

    public function save()
    {
        foreach($this->input->post() as $index=>$value){
            if($index != 'id_'){
                $DataForm[$index]=$value;
            }
        }

        if(empty($this->input->post('id_'))){
            $DataForm['password']=md5($this->input->post('nis'));
            $this->db->insert('tb_siswa', $DataForm);

            $Response =[
                'success'=>true,
                'status'=>"OK",
                'message'=>"Data siswa Berhasil Di Tambahkan",
                'data'=>$DataForm
            ];
        }else{
            $this->db->where('id_siswa', $this->input->post('id_'));
            $this->db->update('tb_siswa', $DataForm);
            
            $Response =[
                'success'=>true,
                'status'=>"OK",
                'message'=>"Data siswa Berhasil Di Ubah",
                'data'=>$DataForm
            ];
        }
        

        $this->output->set_content_type('application/json')->set_output(json_encode($Response));
        
    }

    public function delete()
    {
        // $RelasiData = $this->db->get_where('tb_siswa', ['id_siswa'=>$this->input->post('id')])->row();

        // if($RelasiData){
        //     $Response =[
        //         'success'=>false,
        //         'status'=>"Gagal",
        //         'message'=>"Data siswa ". $RelasiData->nama_siswa . "Tidak dapat dihapus, sedang digunakan",
        //         'data'=>''
        //     ];
        // }else{
            $this->db->where('id_siswa', $this->input->post('id'));
            $this->db->delete('tb_siswa');
            
            $Response =[
                'success'=>true,
                'status'=>"OK",
                'message'=>"Data siswa Berhasil Di hapus",
                'data'=>''
            ];
        // }
        
        $this->output->set_content_type('application/json')->set_output(json_encode($Response));
        
    }

    public function emptydata(Type $var = null)
    {
        $this->db->empty_table('tb_siswa');
        $Response =[
            'success'=>true,
            'status'=>"OK",
            'message'=>"Data Berhasil Di kosongkan",
            'data'=>''
        ];

        $this->output->set_content_type('application/json')->set_output(json_encode($Response));
        
    }
}

/* End of file Siswa.php and path \application\controllers\Siswa.php */