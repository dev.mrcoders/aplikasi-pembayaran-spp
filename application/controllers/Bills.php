<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Bills extends MY_Controller {

    protected $data;
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->data['title']='Data Tagihan';
        $this->view('bills.index',$this->data);
        // $this->output->set_content_type('application/json')->set_output(json_encode($this->data));
    }

    public function Data()
    {
        
        if($this->input->get('id')){
            $this->db->where('id_tagihan', $this->input->get('id'));
        }
        
        $this->db->like('jenis_tagihan',$this->input->get('search'),'BOTH');
        $list = $this->db->get('tb_tagihan');

        $result = ($this->input->get('id') ? $list->row():$list->result() );

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
        
    }

    public function save()
    {
        foreach($this->input->post() as $index=>$value){
            if($index != 'id_'){
                if($index == 'total_tagihan'){
                    $val = explode(',',$value);
                    $DataForm['total_tagihan']=join('',$val);
                }else{
                    $DataForm[$index]=$value;
                }
                
            }
        }

        $DataForm['active']=($this->input->post('active') == 'on' ? 'Y':'N');


        if(empty($this->input->post('id_'))){
            $this->db->insert('tb_tagihan', $DataForm);

            $Response =[
                'success'=>true,
                'status'=>"OK",
                'message'=>"Data tagihan Berhasil Di Tambahkan",
                'data'=>$DataForm
            ];
        }else{
            $this->db->where('id_tagihan', $this->input->post('id_'));
            $this->db->update('tb_tagihan', $DataForm);
            
            $Response =[
                'success'=>true,
                'status'=>"OK",
                'message'=>"Data tagihan Berhasil Di Ubah",
                'data'=>$DataForm
            ];
        }
        

        $this->output->set_content_type('application/json')->set_output(json_encode($Response));
        
    }

    public function delete()
    {
        // $RelasiData = $this->db->get_where('tb_siswa', ['id_siswa'=>$this->input->post('id')])->row();

        // if($RelasiData){
        //     $Response =[
        //         'success'=>false,
        //         'status'=>"Gagal",
        //         'message'=>"Data siswa ". $RelasiData->nama_siswa . "Tidak dapat dihapus, sedang digunakan",
        //         'data'=>''
        //     ];
        // }else{
            $this->db->where('id_tagihan', $this->input->post('id'));
            $this->db->delete('tb_tagihan');
            
            $Response =[
                'success'=>true,
                'status'=>"OK",
                'message'=>"Data Tagihan Berhasil Di hapus",
                'data'=>''
            ];
        // }
        
        $this->output->set_content_type('application/json')->set_output(json_encode($Response));
        
    }

    public function emptydata(Type $var = null)
    {
        $this->db->empty_table('tb_tagihan');
        $Response =[
            'success'=>true,
            'status'=>"OK",
            'message'=>"Data Berhasil Di kosongkan",
            'data'=>''
        ];

        $this->output->set_content_type('application/json')->set_output(json_encode($Response));
        
    }
}

/* End of file Bilss.php and path \application\controllers\Bilss.php */