<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Main extends MY_Controller {
 
    function __construct()
    {
        parent::__construct();
 
    }
 
    public function index()
    {
        echo "<h1>Welcome to the world of Codeigniter</h1>";//Just an example to ensure that we get into the function
        die();
    }

    public function employees()
    {
        $crud = new grocery_CRUD();
        // pilih tabel yang akan digunakan
        $crud->set_table('employees');
        // simpan hasilnya kedalam variabel output
        $output = $crud->render();
 
        $this->load->view('example', $output);
    }
}