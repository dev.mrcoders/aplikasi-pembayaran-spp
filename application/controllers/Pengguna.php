<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengguna extends MY_Controller {

    protected $data;
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->data['title']='Data Pengguna Pengelola';
        $this->view('pengguna.index',$this->data);
        // $this->output->set_content_type('application/json')->set_output(json_encode($this->data));
    }

    public function Data()
    {
        
        if($this->input->get('id')){
            $this->db->where('b.id_user', $this->input->get('id'));
        }
        $this->db->join('tb_role_user a', 'b.role_id=a.id_role', 'inner');
        $list = $this->db->get('tb_user b');

        $result = ($this->input->get('id') ? $list->row():$list->result() );

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
        
    }

    public function ListRole()
    {
        $Result = $this->db->get('tb_role_user')->result();

        $this->output->set_content_type('application/json')->set_output(json_encode($Result));
        
        
    }

    public function save()
    {
        $DataForm = [
            'nama'=>$this->input->post('nama'),
            'username'=>$this->input->post('username'),
            'role_id'=>$this->input->post('role')
        ];

        $config['upload_path'] = './assets/img/';
        $config['allowed_types'] = 'jpeg|jpg|png';
        
        $this->load->library('upload', $config);
        
        if(!empty($_FILES['foto']['name'])){
            if ( ! $this->upload->do_upload('foto')){
                $error = $this->upload->display_errors();
                $Response =[
                            'success'=>false,
                            'status'=>"error",
                            'message'=>$error,
                            'data'=>''
                        ];
            }
            else{
                $data =$this->upload->data();
                $DataForm['foto']=$data['file_name'];
                
            }
        }

        
        if(empty($this->input->post('id_'))){
            $DataForm['password']=md5($this->input->post('password'));
            $this->db->insert('tb_user', $DataForm);

            $Response =[
                'success'=>true,
                'status'=>"OK",
                'message'=>"Data User Berhasil Di Tambahkan",
                'data'=>$DataForm
            ];
        }else{
            
            if(!empty($this->input->post('password'))){
                $DataForm['password']=md5($this->input->post('password'));
            }

            $this->db->where('id_user', $this->input->post('id_'));
            $this->db->update('tb_user', $DataForm);
            $Response =[
                'success'=>true,
                'status'=>"OK",
                'message'=>"Data User Berhasil Di Ubah",
                'data'=>$DataForm
            ];
        }
        

        $this->output->set_content_type('application/json')->set_output(json_encode($Response));
        
    }

    public function delete()
    {
        // $RelasiData = $this->db->get_where('tb_siswa', ['id_siswa'=>$this->input->post('id')])->row();

        // if($RelasiData){
        //     $Response =[
        //         'success'=>false,
        //         'status'=>"Gagal",
        //         'message'=>"Data siswa ". $RelasiData->nama_siswa . "Tidak dapat dihapus, sedang digunakan",
        //         'data'=>''
        //     ];
        // }else{
            $this->db->where('id_user', $this->input->post('id'));
            $this->db->delete('tb_user');
            
            $Response =[
                'success'=>true,
                'status'=>"OK",
                'message'=>"Data User Berhasil Di hapus",
                'data'=>''
            ];
        // }
        
        $this->output->set_content_type('application/json')->set_output(json_encode($Response));
        
    }

    public function emptydata(Type $var = null)
    {
        $this->db->empty_table('tb_tagihan');
        $Response =[
            'success'=>true,
            'status'=>"OK",
            'message'=>"Data Berhasil Di kosongkan",
            'data'=>''
        ];

        $this->output->set_content_type('application/json')->set_output(json_encode($Response));
        
    }
}

/* End of file Pengguna.php and path \application\controllers\Pengguna.php */