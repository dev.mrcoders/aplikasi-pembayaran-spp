<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    public function index($page=null)
    {
        if($page == null){
            $this->view('login_siswa');
        }else{
            $this->view('login');
        }
        
    }

    public function CheckLogin()
    {
        $FormUser = [
            'username'=>$this->input->post('username'),
            'password'=>md5($this->input->post('password'))
        ];

        $this->db->join('tb_role_user a', 'a.id_role= b.role_id', 'inner');
        $ChecKaccount = $this->db->get_where('tb_user b', $FormUser)->row();

        if($ChecKaccount){
            $array = array(
                'username' => $ChecKaccount->username,
                'name' => $ChecKaccount->nama,
                'role'=>$ChecKaccount->nama_role,
                'level'=>$ChecKaccount->id_role,
                'foto'=>$ChecKaccount->foto,
                'login'=>TRUE
            );
            
            $this->session->set_userdata( $array );
            
            $Response =[
                'success'=>true,
                'status'=>"OK",
                'message'=>"Login Berhasil",
                'data'=>[
                    'redirect'=>'dashboard'
                ]
            ];
        }else{
            $Response =[
                'success'=>false,
                'status'=>"error",
                'message'=>"Username or Password invalid",
                'data'=>[
                    'redirect'=>''
                ]
            ];
        }
        
        $this->output->set_content_type('application/json')->set_output(json_encode($Response));
        
        
    }

    public function CheckUserSiswa()
    {
        $FormUser = [
            'nis'=>$this->input->post('username'),
            'password'=>md5($this->input->post('password'))
        ];

        $ChecKaccount = $this->db->get_where('tb_siswa', $FormUser)->row();

        if($ChecKaccount){
            $array = array(
                'username' => $ChecKaccount->nis,
                'name' => $ChecKaccount->nama_siswa,
                'role'=>'siswa',
                'level'=>'0',
                'foto'=>'',
                'login'=>TRUE
            );
            
            $this->session->set_userdata( $array );
            
            $Response =[
                'success'=>true,
                'status'=>"OK",
                'message'=>"Login Berhasil",
                'data'=>[
                    'redirect'=>'dashboard'
                ]
            ];
        }else{
            $Response =[
                'success'=>false,
                'status'=>"error",
                'message'=>"Username or Password invalid",
                'data'=>[
                    'redirect'=>''
                ]
            ];
        }
        
        $this->output->set_content_type('application/json')->set_output(json_encode($Response));
        
        
    }

    public function Logout(Type $var = null)
    {
        $array = array(
            'username' => '',
            'name' => '',
            'role'=>'',
            'level'=>'',
            'foto'=>'',
            'login'=>FALSE
        );
      	$this->session->unset_userdata($array);//clear session
      	$this->session->sess_destroy();//tutup session
      	redirect(base_url());
      }
  }

/* End of file Login.php and path \application\controllers\Login.php */