<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kelas extends MY_Controller
{

    protected $data;
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->data['title']='Data Kelas';
        $this->view('kelas.index',$this->data);
        // $this->output->set_content_type('application/json')->set_output(json_encode($this->data));
    }

    public function Data()
    {
        if($this->input->get('id')){
            $this->db->where('id_kelas', $this->input->get('id'));
        }
        $list = $this->db->get('tb_kelas');

        $result = ($this->input->get('id') ? $list->row():$list->result() );

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
        
    }

    public function save()
    {
        $DataForm = [
            'nama_kelas'=>$this->input->post('kelas')
        ];

        if(empty($this->input->post('id_'))){
            $this->db->insert('tb_kelas', $DataForm);

            $Response =[
                'success'=>true,
                'status'=>"OK",
                'message'=>"Data Kelas Berhasil Di Tambahkan",
                'data'=>$DataForm
            ];
        }else{
            $this->db->where('id_kelas', $this->input->post('id_'));
            $this->db->update('tb_kelas', $DataForm);
            
            $Response =[
                'success'=>true,
                'status'=>"OK",
                'message'=>"Data Kelas Berhasil Di Ubah",
                'data'=>$DataForm
            ];
        }
        

        $this->output->set_content_type('application/json')->set_output(json_encode($Response));
        
    }

    public function delete()
    {
        $RelasiData = $this->db->get_where('tb_siswa', ['id_kelas'=>$this->input->post('id')])->row();

        if($RelasiData){
            $Response =[
                'success'=>false,
                'status'=>"Gagal",
                'message'=>"Data Kelas ". $RelasiData->nama_kelas . "Tidak dapat dihapus, sedang digunakan",
                'data'=>''
            ];
        }else{
            $this->db->where('id_kelas', $this->input->post('id'));
            $this->db->delete('tb_kelas');
            
            $Response =[
                'success'=>true,
                'status'=>"OK",
                'message'=>"Data Kelas Berhasil Di hapus",
                'data'=>''
            ];
        }
        
        $this->output->set_content_type('application/json')->set_output(json_encode($Response));
        
    }

    public function emptydata(Type $var = null)
    {
        $this->db->empty_table('tb_kelas');
        $Response =[
            'success'=>true,
            'status'=>"OK",
            'message'=>"Data Berhasil Di kosongkan",
            'data'=>''
        ];

        $this->output->set_content_type('application/json')->set_output(json_encode($Response));
        
    }
}

/* End of file Kelas.php and path \application\controllers\Kelas.php */