<?php 
Route::get('default_controller', 'login', array());
Route::get('admins', 'login/index/admins', array());
Route::post('check-login', 'login/CheckLogin', array());
Route::post('login-siswa', 'login/checkusersiswa', array());
Route::get('sign-out', 'login/logout', array());
Route::get('dashboard', 'dashboard', array());
Route::get('dashboard/stats', 'dashboard/statistik', array());



Route::any('data-kelas', 'kelas/index', array(),function(){
    Route::match(['POST','GET'],'tables','kelas/data',array());
    Route::post('save','kelas/save',array());
    Route::get('empty-data','kelas/emptydata',array());
    Route::post('delete','kelas/delete',array());
});

Route::any('data-angkatan', 'angkatan/index', array(),function(){
    Route::match(['POST','GET'],'tables','angkatan/data',array());
    Route::post('save','angkatan/save',array());
    Route::get('empty-data','angkatan/emptydata',array());
    Route::post('delete','angkatan/delete',array());
});

Route::any('data-siswa', 'siswa/index', array(),function(){
    Route::match(['POST','GET'],'tables','siswa/data',array());
    Route::post('save','siswa/save',array());
    Route::get('empty-data','siswa/emptydata',array());
    Route::post('delete','siswa/delete',array());
});

Route::any('data-tagihan', 'bills/index', array(),function(){
    Route::match(['POST','GET'],'tables','bills/data',array());
    Route::post('save','bills/save',array());
    Route::get('empty-data','bills/emptydata',array());
    Route::post('delete','bills/delete',array());
});


Route::any('data-pembayaran', 'pembayaran/index', array(),function(){
    Route::match(['POST','GET'],'tables','pembayaran/data',array());
    Route::post('save','pembayaran/save',array());
    Route::get('empty-data','pembayaran/emptydata',array());
    Route::post('delete','pembayaran/delete',array());
});

Route::any('data-rekap', 'pembayaran/rekap', array(),function(){
    Route::match(['POST','GET'],'tables','pembayaran/datarekap',array());
    Route::post('save','pembayaran/save',array());
    Route::get('statistik','pembayaran/datastatistik',array());
    Route::get('stats-bayar','pembayaran/statistikbysiswa',array());
    Route::post('delete','pembayaran/delete',array());
    Route::get('statistikbysiswa','pembayaran/statistikjumlahbayar',array());

});

Route::any('data-pengguna', 'pengguna/index', array(),function(){
    Route::match(['POST','GET'],'tables','pengguna/data',array());
    Route::post('save','pengguna/save',array());
    Route::post('delete','pengguna/delete',array());
});


Route::any('data-role', 'pengguna/role', array(),function(){
    Route::match(['POST','GET'],'tables','pengguna/listrole',array());
    Route::post('save','pengguna/save',array());
    Route::get('statistik','pengguna/datastatistik',array());
    Route::post('delete','pengguna/delete',array());
});

Route::any('data-sekolah', 'profile/index', array(),function(){
    Route::match(['POST','GET'],'tables','pengguna/listrole',array());
    Route::post('save','profile/save',array());
    Route::get('detail','profile/detailsekolah',array());
});

Route::any('pengaturan', 'profile/pengaturan', array(),function(){
    Route::match(['POST','GET'],'tables','pengguna/listrole',array());
    Route::post('save','profile/savepengaturan',array());
    Route::get('detail','profile/detailsistem',array());
});

?>