-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 02 Feb 2023 pada 21.02
-- Versi server: 5.7.33
-- Versi PHP: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_spp_pesantren`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_angkatan`
--

CREATE TABLE `tb_angkatan` (
  `id_angkatan` int(5) NOT NULL,
  `nama_angkatan` varchar(25) NOT NULL,
  `tahun` varchar(10) NOT NULL,
  `semester` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_angkatan`
--

INSERT INTO `tb_angkatan` (`id_angkatan`, `nama_angkatan`, `tahun`, `semester`) VALUES
(3, 'covid', '2021', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kelas`
--

CREATE TABLE `tb_kelas` (
  `id_kelas` int(5) NOT NULL,
  `nama_kelas` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_kelas`
--

INSERT INTO `tb_kelas` (`id_kelas`, `nama_kelas`) VALUES
(8, 'AB');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pembayaran`
--

CREATE TABLE `tb_pembayaran` (
  `id_pembayaran` int(5) NOT NULL,
  `id_tagihan` int(5) NOT NULL,
  `id_siswa` int(5) NOT NULL,
  `tgl_bayar` datetime NOT NULL,
  `bln_thn_bayar` varchar(15) NOT NULL,
  `jumlah_pembayaran` varchar(25) NOT NULL,
  `sisa` varchar(25) NOT NULL,
  `status` int(5) NOT NULL,
  `user_created` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_pembayaran`
--

INSERT INTO `tb_pembayaran` (`id_pembayaran`, `id_tagihan`, `id_siswa`, `tgl_bayar`, `bln_thn_bayar`, `jumlah_pembayaran`, `sisa`, `status`, `user_created`) VALUES
(1, 2, 7, '2023-01-25 14:26:25', '2023-01', '10000000', '15000000', 0, 'Admin'),
(2, 2, 7, '2023-01-25 14:29:20', '2023-02', '10000000', '5000000', 0, 'Admin'),
(3, 2, 7, '2023-01-25 14:35:38', '2023-03', '5000000', '0', 1, 'Admin'),
(8, 3, 7, '2023-01-26 03:20:29', '2023-01', '200000', '0', 1, 'Admin'),
(9, 4, 7, '2023-01-23 03:22:38', '2023-01', '500000', '0', 1, 'Admin'),
(10, 2, 8, '2023-01-26 06:59:45', '2023-01', '10000000', '15000000', 0, 'Admin'),
(11, 2, 8, '2023-01-26 07:04:07', '2023-02', '5000000', '10000000', 0, 'Administrator');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pengaturan`
--

CREATE TABLE `tb_pengaturan` (
  `id_pengaturan` int(5) NOT NULL,
  `bg_header` varchar(25) NOT NULL,
  `color_text` varchar(25) NOT NULL,
  `bg_aside` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_pengaturan`
--

INSERT INTO `tb_pengaturan` (`id_pengaturan`, `bg_header`, `color_text`, `bg_aside`) VALUES
(1, '#44bec1', '#ffffff', '#44bec1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_profile_sekolah`
--

CREATE TABLE `tb_profile_sekolah` (
  `id_sekolah` int(5) NOT NULL,
  `nama_sekolah` varchar(45) NOT NULL,
  `alamat` text NOT NULL,
  `website` varchar(128) NOT NULL,
  `email` varchar(75) NOT NULL,
  `telepon` varchar(25) NOT NULL,
  `pimpinan` varchar(45) NOT NULL,
  `kepsek_mts` varchar(45) NOT NULL,
  `kepsek_ma` varchar(45) NOT NULL,
  `logo` varchar(128) NOT NULL,
  `deskripsi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_profile_sekolah`
--

INSERT INTO `tb_profile_sekolah` (`id_sekolah`, `nama_sekolah`, `alamat`, `website`, `email`, `telepon`, `pimpinan`, `kepsek_mts`, `kepsek_ma`, `logo`, `deskripsi`) VALUES
(1, 'Ponpes Darusalam ', 'jl ....', '......', '....', '090903', 'tesss', 'tess', 'tesss', 'tutwuri.png', 'tesssss dfafds adfd');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_role_user`
--

CREATE TABLE `tb_role_user` (
  `id_role` int(5) NOT NULL,
  `nama_role` varchar(45) NOT NULL,
  `keterangan` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_role_user`
--

INSERT INTO `tb_role_user` (`id_role`, `nama_role`, `keterangan`) VALUES
(1, 'Administrator', ''),
(2, 'User', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_siswa`
--

CREATE TABLE `tb_siswa` (
  `id_siswa` int(5) NOT NULL,
  `nis` varchar(25) NOT NULL,
  `nisn` varchar(25) NOT NULL,
  `nama_siswa` varchar(45) NOT NULL,
  `kelamin` enum('P','L') NOT NULL,
  `nama_ayah` varchar(45) NOT NULL,
  `nama_ibu` varchar(45) NOT NULL,
  `alamat` varchar(75) NOT NULL,
  `id_kelas` int(5) NOT NULL,
  `id_angkatan` int(5) NOT NULL,
  `password` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_siswa`
--

INSERT INTO `tb_siswa` (`id_siswa`, `nis`, `nisn`, `nama_siswa`, `kelamin`, `nama_ayah`, `nama_ibu`, `alamat`, `id_kelas`, `id_angkatan`, `password`) VALUES
(7, '123456', '4354sfe', 'aku', 'P', '8VrwTeDmSf', 'mNe9hl2qsX', 'asdf', 8, 3, 'e10adc3949ba59abbe56e057f20f883e'),
(8, '654321', '3454333', 'Testtt', 'L', 'sfeewe', 'sdfsdf', 'seasf', 8, 3, 'c33367701511b4f6020ec61ded352059');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_tagihan`
--

CREATE TABLE `tb_tagihan` (
  `id_tagihan` int(5) NOT NULL,
  `jenis_tagihan` varchar(25) NOT NULL,
  `total_tagihan` varchar(25) NOT NULL,
  `active` enum('Y','N') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_tagihan`
--

INSERT INTO `tb_tagihan` (`id_tagihan`, `jenis_tagihan`, `total_tagihan`, `active`) VALUES
(2, 'SPP', '25000000', 'Y'),
(3, 'Uang Ujian', '200000', 'Y'),
(4, 'Uang Baju', '500000', 'Y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(5) NOT NULL,
  `nama` varchar(45) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(75) NOT NULL,
  `foto` varchar(45) DEFAULT NULL,
  `role_id` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `nama`, `username`, `password`, `foto`, `role_id`) VALUES
(1, 'Administrator', 'Administrator', 'e10adc3949ba59abbe56e057f20f883e', NULL, 1),
(5, 'Pimpinan', 'user1', 'e10adc3949ba59abbe56e057f20f883e', NULL, 2);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tb_angkatan`
--
ALTER TABLE `tb_angkatan`
  ADD PRIMARY KEY (`id_angkatan`);

--
-- Indeks untuk tabel `tb_kelas`
--
ALTER TABLE `tb_kelas`
  ADD PRIMARY KEY (`id_kelas`);

--
-- Indeks untuk tabel `tb_pembayaran`
--
ALTER TABLE `tb_pembayaran`
  ADD PRIMARY KEY (`id_pembayaran`);

--
-- Indeks untuk tabel `tb_pengaturan`
--
ALTER TABLE `tb_pengaturan`
  ADD PRIMARY KEY (`id_pengaturan`);

--
-- Indeks untuk tabel `tb_profile_sekolah`
--
ALTER TABLE `tb_profile_sekolah`
  ADD PRIMARY KEY (`id_sekolah`);

--
-- Indeks untuk tabel `tb_role_user`
--
ALTER TABLE `tb_role_user`
  ADD PRIMARY KEY (`id_role`);

--
-- Indeks untuk tabel `tb_siswa`
--
ALTER TABLE `tb_siswa`
  ADD PRIMARY KEY (`id_siswa`);

--
-- Indeks untuk tabel `tb_tagihan`
--
ALTER TABLE `tb_tagihan`
  ADD PRIMARY KEY (`id_tagihan`);

--
-- Indeks untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tb_angkatan`
--
ALTER TABLE `tb_angkatan`
  MODIFY `id_angkatan` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tb_kelas`
--
ALTER TABLE `tb_kelas`
  MODIFY `id_kelas` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `tb_pembayaran`
--
ALTER TABLE `tb_pembayaran`
  MODIFY `id_pembayaran` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `tb_pengaturan`
--
ALTER TABLE `tb_pengaturan`
  MODIFY `id_pengaturan` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tb_profile_sekolah`
--
ALTER TABLE `tb_profile_sekolah`
  MODIFY `id_sekolah` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tb_role_user`
--
ALTER TABLE `tb_role_user`
  MODIFY `id_role` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tb_siswa`
--
ALTER TABLE `tb_siswa`
  MODIFY `id_siswa` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `tb_tagihan`
--
ALTER TABLE `tb_tagihan`
  MODIFY `id_tagihan` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
